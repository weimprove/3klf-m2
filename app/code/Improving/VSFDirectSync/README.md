Add the repository to composer.json under repositories:

```
    {
      "type": "vcs",
      "url": "https://improvingaps@bitbucket.org/weimprove/improving_vsfdirectsync.git"
    }
```

Like this: 
```
  "repositories": [
    {
      "type": "composer",
      "url": "https://repo.magento.com/"
    },
    {
      "type": "vcs",
      "url": "https://improvingaps@bitbucket.org/weimprove/improving_vsfdirectsync.git"
    }
  ],
```

Require the latests tagged version ex:
```
composer require improving/vsfdirectsync:1.0.1
```
