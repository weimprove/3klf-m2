<?php

namespace Improving\VSFDirectSync\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('vsfdirectsync_attribute_renderer')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('vsfdirectsync_attribute_renderer')
            )
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'identity' => true,
                    ],
                    'ID'
                )
                ->addColumn(
                    'entity_type_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Entity type id'
                )
                ->addColumn(
                    'attribute_code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                        'default'  => '',
                    ],
                    'Attribute code'
                )
                ->addColumn(
                    'renderer',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                        'default'  => '',
                    ],
                    'Renderer'
                )
                ->setComment('VSFDirectSync Attribute Renderers');
            $installer->getConnection()->createTable($table);
        }
    }
}
