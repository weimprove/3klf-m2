<?php

namespace Improving\VSFDirectSync\Console\Command\Export;

use Improving\VSFDirectSync\Model\Sync\Category;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Store\Api\GroupRepositoryInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CategoryCommand extends Command
{
    /**
     * @var State
     */
    private $state;
    /**
     * @var Emulation
     */
    private $emulation;
    private $index;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;
    /**
     * @var Category
     */
    private $category;

    /**
     * CategoryCommand constructor.
     * @param Emulation $emulation
     * @param Category $category
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param GroupRepositoryInterface $groupRepository
     * @param State $state
     */
    public function __construct(
        Emulation $emulation,
        Category $category,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        GroupRepositoryInterface $groupRepository,
        State $state
    ) {
        parent::__construct();
        $this->state           = $state;
        $this->emulation       = $emulation;
        $this->scopeConfig     = $scopeConfig;
        $this->storeManager    = $storeManager;
        $this->groupRepository = $groupRepository;
        $this->category        = $category;
    }

    /**
     * Configures the current command.
     */
    public function configure()
    {
        $this->setName('vsfdirectsync:categories');
        $this->setDescription('improving:vsfdirectsync:Export\Category');
    }

    /**
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     */
    public function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $output->writeln('CategoryCommand');

        echo 'store id => elasticsearch index' . PHP_EOL;
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'websites');
        foreach ($this->storeManager->getStores() as $store) {
            $id   = $store->getId();
            $code = $store->getCode();

            $this->emulation->startEnvironmentEmulation($id);
            $this->index = $index . '_' . $code;
            echo $id . ' => ' . $this->index . PHP_EOL;

            $group = $this->groupRepository->get($store->getStoreGroupId());
            $this->category->indexForStore($id, $group->getRootCategoryId(), $this->index);
        }
    }
}
