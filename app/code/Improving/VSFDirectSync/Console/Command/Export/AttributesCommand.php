<?php

namespace Improving\VSFDirectSync\Console\Command\Export;

use Elasticsearch\ClientBuilder;
use Improving\VSFDirectSync\Helper\Attribute;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AttributesCommand extends Command
{
    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepository;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var State
     */
    private $state;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    private $esClient;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var Attribute
     */
    private $attribute;

    /**
     * AttributesCommand constructor.
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     * @param StoreManagerInterface $storeManager
     * @param Emulation $emulation
     * @param ScopeConfigInterface $scopeConfig
     * @param ResourceConnection $resourceConnection
     * @param State $state
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        StoreManagerInterface $storeManager,
        Emulation $emulation,
        ScopeConfigInterface $scopeConfig,
        ResourceConnection $resourceConnection,
        Attribute $attribute,
        State $state
    ) {
        parent::__construct();
        $this->productAttributeRepository = $productAttributeRepository;
        $this->emulation                  = $emulation;
        $this->state                      = $state;
        $this->esClient                   = ClientBuilder::create()->build();
        $this->searchCriteriaBuilder      = $searchCriteriaBuilder;
        $this->storeManager               = $storeManager;
        $this->scopeConfig                = $scopeConfig;
        $this->resourceConnection         = $resourceConnection;
        $this->attribute                  = $attribute;
    }

    /**
     * Configures the current command.
     */
    public function configure()
    {
        $this->setName('vsfdirectsync:attributes');
        $this->setDescription('improving:vsfdirectsync:Export\Attributes');
    }

    /**
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     */
    public function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        $output->writeln('AttributesCommand');

        echo 'store id => elasticsearch index' . PHP_EOL;
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'websites');
        foreach ($this->storeManager->getStores() as $store) {
            $id   = $store->getId();
            $code = $store->getCode();

            $this->emulation->startEnvironmentEmulation($id);
            $this->index = $index . '_' . $code;
            echo $id . ' => ' . $this->index . PHP_EOL;
            $this->indexForStore($id, $this->index);
        }
    }

    protected function indexForStore($storeId, $index)
    {
        $this->emulation->startEnvironmentEmulation($storeId);
        $this->index = $index;

        $search = $this->searchCriteriaBuilder->create();
        $list   = $this->productAttributeRepository->getList($search);

        foreach ($list->getItems() as $attribute) {
            $data = $this->attribute->extractAttributeData($storeId, $attribute);
            $data = $this->attribute->optimizeAttributeOptions($data, $index);

            $params = [
                'index' => $this->index,
                'type'  => 'attribute',
                'id'    => $data['id'],
                'body'  => $data
            ];

            $response = $this->esClient->index($params);
        }
    }
}

