<?php

namespace Improving\VSFDirectSync\Console\Command\Export;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Api\TaxRuleRepositoryInterface;
use Elasticsearch\ClientBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\State;
use Magento\Store\Model\App\Emulation;
use Magento\Tax\Api\TaxRateRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Improving\VSFDirectSync\Helper\Data;

class TaxRulesCommand extends Command
{
    protected $booleans = [
        'calculate_subtotal'
    ];

    private $esClient;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var State
     */
    private $state;
    /**
     * @var TaxRateRepositoryInterface
     */
    private $rateRepository;
    /**
     * @var TaxRuleRespositoryInterface
     */
    private $ruleRespository;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    private $index;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * TaxRulesCommand constructor.
     * @param Emulation $emulation
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param State $state
     * @param Data $helper
     * @param ScopeConfigInterface $scopeConfig
     * @param TaxRuleRepositoryInterface $ruleRepository
     * @param StoreManagerInterface $storeManager
     * @param TaxRateRepositoryInterface $rateRepository
     * @internal param TaxRuleRepositoryInterface $ruleRespository
     * @internal param TaxRateRepositoryInterface $rateRepository
     */
    public function __construct(
        Emulation $emulation,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        State $state,
        Data $helper,
        ScopeConfigInterface $scopeConfig,
        TaxRuleRepositoryInterface $ruleRepository,
        StoreManagerInterface $storeManager,
        TaxRateRepositoryInterface $rateRepository
    ) {
        parent::__construct();
        $this->esClient = ClientBuilder::create()->build();
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->emulation = $emulation;
        $this->state = $state;
        $this->ruleRespository = $ruleRepository;
        $this->rateRepository = $rateRepository;
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Configures the current command.
     */
    public function configure()
    {
        $this->setName('vsfdirectsync:taxrules');
        $this->setDescription('improving:vsfdirectsync:Export\TaxRules');
    }

    /**
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     */
    public function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $output->writeln('TaxRulesCommand');
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);

        echo 'store id => elasticsearch index' . PHP_EOL;
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'websites');
        foreach ($this->storeManager->getStores() as $store) {
            $id = $store->getId();
            $code = $store->getCode();

            $this->emulation->startEnvironmentEmulation($id);
            $this->index = $index . '_' . $code;
            echo $id . ' => ' . $this->index . PHP_EOL;
            $this->indexForStore($id, $this->index);
        }
    }

    protected function indexForStore($storeId, $index)
    {
        $this->emulation->startEnvironmentEmulation($storeId);
        $this->index = $index;

        $search = $this->searchCriteriaBuilder->create();
        $list = $this->ruleRespository->getList($search);

        foreach ($list->getItems() as $taxRule) {
            $data = $taxRule->getData();
            $data['id'] = $taxRule->getId();

            $data['tax_rate_ids'] = implode(',', $taxRule->getTaxRateIds());
            $rateSearch = $this->searchCriteriaBuilder->addFilter('tax_calculation_rate_id', $data['tax_rate_ids'], 'in')->create();

            $rateList = $this->rateRepository->getList($rateSearch);
            $data['rates'] = [];

            foreach ($rateList->getItems() as $taxRate) {
                $titles = [];

                if ($taxRate->getTitles()) {
                    foreach ($taxRate->getTitles() as $title) {
                        $titles[] = [
                            'store_id' => $title->getStoreId(),
                            'value'    => $title->getValue(),
                        ];
                    }
                }

                $data['rates'][] = [
                    'tax_region_id'  => $taxRate->getTaxRegionId(),
                    'code'           => $taxRate->getCode(),
                    'rate'           => $taxRate->getRate(),
                    'id'             => $taxRate->getId(),
                    'tax_country_id' => $taxRate->getTaxCountryId(),
                    'titles'         => $titles,
                    'tax_postcode'   => $taxRate->getTaxPostcode(),
                ];
            }

            unset($data['tax_rates_codes']);
            unset($data['product_tax_classes']);
            unset($data['customer_tax_classes']);
            unset($data['tax_calculation_rule_id']);
            unset($data['tax_rates']);

            $data['customer_tax_class_ids'] = implode(',', $taxRule->getCustomerTaxClassIds());
            $data['product_tax_class_ids'] = implode(',', $taxRule->getProductTaxClassIds());

            array_walk_recursive($data, [$this->helper, 'convertBooleans'], [$this->booleans]);

            $params = [
                'index' => $this->index,
                'type'  => 'taxrule',
                'id'    => $data['id'],
                'body'  => $data
            ];

            $response = $this->esClient->index($params);
        }
    }

}

