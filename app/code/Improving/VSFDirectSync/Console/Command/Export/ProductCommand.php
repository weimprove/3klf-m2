<?php

namespace Improving\VSFDirectSync\Console\Command\Export;

use Improving\VSFDirectSync\Helper\Data;
use Improving\VSFDirectSync\Model\Indexer\Product\DataProvider;
use Improving\VSFDirectSync\Model\Indexer\Product\Resolver;
use Improving\VSFDirectSync\Model\Indexer\Product\Saver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCommand extends Command
{
    /**
     * @var Resolver
     */
    protected $resolver;
    /**
     * @var DataProvider
     */
    protected $dataProvider;
    /**
     * @var Saver
     */
    protected $saver;
    /**
     * @var Improving\VSFDirectSync\Helper\Data
     */
    protected $helper;

    /**
     * NewProductIndexerCommand constructor.
     * @param Resolver $resolver
     * @param DataProvider $dataProvider
     * @param Saver $saver
     * @param Improving\VSFDirectSync\Helper\Data $helper
     * @param null $name
     */
    public function __construct(
        Resolver $resolver,
        DataProvider $dataProvider,
        Saver $saver,
        Data $helper,
        $name = null
    ) {
        parent::__construct($name);
        $this->resolver     = $resolver;
        $this->dataProvider = $dataProvider;
        $this->saver        = $saver;
        $this->helper       = $helper;
    }

    /**
     * Configures the current command.
     */
    public function configure()
    {
        $options = [
            new InputOption(
                'id',
                null,
                InputOption::VALUE_OPTIONAL,
                'Single Id'
            ),
            new InputOption(
                'storeId',
                null,
                InputOption::VALUE_OPTIONAL,
                'Single Store id'
            ),
        ];
        $this->setName('vsfdirectsync:products');
        $this->setDescription('improving:vsfdirectsync:newProductIndexer');
        $this->setDefinition($options);
    }

    /**
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     */
    public function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $output->writeln('NewProductIndexerCommand');

        foreach ($this->helper->getStoresToIndex() as $store) {
            $websiteId = $store->getWebsiteId();
            $storeId   = $store->getStoreId();

            if ($input->getOption('storeId') && $storeId != $input->getOption('storeId')) {
                continue;
            }
            if ($input->getOption('id')) {
                $productIds = [$input->getOption('id')];
            } else {
                $productIds = $this->resolver->getProductIds($websiteId, $storeId);
            }

            $this->dataProvider->rebuildStoreIndex($websiteId, $storeId, $productIds, $this->saver);

            if (!$input->getOption('id')) {
                $this->helper->cleanUpTheRest($this->saver->getIndexedIds($storeId), $storeId);
            }
        }
    }
}

