<?php

namespace Improving\VSFDirectSync\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductAttributeSchemaCommand extends Command
{

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
    ) {
        parent::__construct();
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Configures the current command.
     */
    public function configure()
    {
        $this->setName('vsfdirectsync:productAttributeSchema');
        $this->setDescription('vsfdirectsync:productAttributeSchema');
    }

    /**
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     * @return null|int null or 0 if everything went fine, or an error code
     */
    public function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        $output->writeln('ProductAttributeSchemaCommand');

        $collection = $this->collectionFactory->create();
        $collection->addFieldtoFilter('entity_type_id', 4);

        $export = [];
        foreach ($collection as $item) {
            if ($item->getBackendType() == 'int' &&
                $item->getIsUserDefined()) {
                $export[$item->getAttributeCode()] = ['type' => 'integer'];
            }


        }

        echo json_encode($export, JSON_PRETTY_PRINT);

    }

}

