<?php

namespace Improving\VSFDirectSync\Helper\Cms;

class Widget
{
    public function convertWidgetFieldsData(&$categoryData)
    {
        $re = '/{{(.*)}}/m';
        foreach ($categoryData as $field => $data) {
            if (is_string($data) && strpos($data, '{{widget') === 0) {
                preg_match_all($re, $data, $matches, PREG_SET_ORDER, 0);
                $widget = "<" . $matches[0][1] . "></widget>";
                $json   = $this->XML2JSON($widget);

                $params = json_decode($json, 1);
                $result = $this->parseWidgetParameters($params);

                $widgetData                      = [];
                $widgetData['instance_type']     = $result['type'];
                $widgetData['widget_parameters'] = $result;
                $categoryData[$field]            = $widgetData;
            }
        }
    }

    function XML2JSON($xml)
    {
        $this->normalizeSimpleXML(simplexml_load_string($xml), $result);
        return json_encode($result);
    }

    function normalizeSimpleXML($obj, &$result)
    {
        $data = $obj;
        if (is_object($data)) {
            $data = get_object_vars($data);
        }
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $res = null;
                $this->normalizeSimpleXML($value, $res);
                if (($key == '@attributes') && ($key)) {
                    $result = $res;
                } else {
                    $result[$key] = $res;
                }
            }
        } else {
            $result = $data;
        }
    }

    public function parseWidgetParameters($params)
    {
        $strings = [];

        foreach ($params as $key => $value) {
            if ($this->isBase64Encoded($value)) {
                $value = utf8_encode(base64_decode(str_replace('base64--', '', $value)));
            }

            $value    = $this->specialEncodeHTML($value);
            $keyParts = explode('__', $key);

            foreach ($keyParts as $keyIndex => $part) {
                if ($keyIndex) {
                    $keyParts[$keyIndex] = "[${part}]";
                }
            }
            // Unescape double quotes. '"' -> html special chars id: 3
            $value = str_replace('\|-3-|', '|-3-|', $value);

            $strings[] = implode('', $keyParts) . '=' . $value;
        }

        parse_str(implode('&', $strings), $result);
        $result = $this->arrayMapRecursive([$this, 'specialDecodeHTML'], $result);

        return $result;
    }

    private function isBase64Encoded($data)
    {
        return strstr($data, 'base64--');
    }

    /**
     * Encode HTML with special strings representing HTML entities
     * @param $content
     * @return mixed
     */
    private function specialEncodeHTML($content)
    {
        $content = htmlspecialchars($content);
        $table   = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES | ENT_HTML5);
        $table   = array_values($table);

        $specialTable = [];
        foreach ($table as $key => $ent) {
            $specialKey                = '|-' . $key . '-|';
            $specialTable[$specialKey] = $ent;
        }

        $converted = str_replace($specialTable, array_keys($specialTable), $content);
        return $converted;
    }

    private function arrayMapRecursive(callable $func, array $array)
    {
        return filter_var($array, \FILTER_CALLBACK, ['options' => $func]);
    }

    /**
     * Extract widget elements from Magento WYSIWYG field
     *
     * @param $content
     * @return array|bool
     */
    public function parseCmsPageWidgetData($content)
    {
        $re = '/{{widget(.*?)}}/m';

        preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);

        if (!count($matches)) {
            return false;
        }

        $withoutWidgets = preg_replace($re, '||widget||', $content);

        libxml_use_internal_errors(true);
        $dom = new \DOMDocument('1.0', 'utf-8');

        $dom->preserveWhiteSpace = false;
        $dom->formatOutput       = true;
        $dom->loadHTML(mb_convert_encoding($withoutWidgets, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $elements = [];
        foreach ($dom->firstChild->childNodes as $node) {
            $html = $dom->saveHTML($node);

            if (strstr($html, '||widget||')) {
                $widget = array_shift($matches);

                $result = $this->extractWidgetParametersFromAttributeString($widget[1]);

                $data['instance_type']     = $result['type'];
                $data['widget_parameters'] = $result;

                $elements[] = [
                    'type' => 'widget',
                    'data' => $data,
                ];
            } else {
                $elements[] = [
                    'type' => 'node',
                    'data' => $html
                ];
            }
        }

        return count($elements) ? $elements : false;
    }

    /**
     * Parse Widget element attribute string
     *
     * @param $string
     * @return array|mixed
     */
    protected function extractWidgetParametersFromAttributeString($string)
    {
        $re = '/(?<name>\b\w+\b)\s*=\s*(?<value>"[^"]*"|\'[^\']*\'|[^"\'<>\s]+)/m';

        preg_match_all($re, $string, $matches, PREG_SET_ORDER, 0);

        $paramsArray = [];
        foreach ($matches as $match) {
            $value                       = trim($match['value'], '"');
            $value                       = html_entity_decode($value);
            $paramsArray[$match['name']] = $value;
        }

        $paramsArray = $this->parseWidgetParameters($paramsArray);

        return $paramsArray;
    }

    public function specialDecodeHTML($content)
    {
        $table = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES | ENT_HTML5);
        $table = array_values($table);

        $specialTable = [];

        foreach ($table as $key => $ent) {
            $specialKey                = '|-' . $key . '-|';
            $specialTable[$specialKey] = $ent;
        }

        $converted = str_replace(array_keys($specialTable), $specialTable, $content);
        return html_entity_decode($converted);
    }
}

