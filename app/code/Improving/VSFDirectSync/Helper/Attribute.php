<?php

namespace Improving\VSFDirectSync\Helper;

use Magento\Framework\App\ResourceConnection;

class Attribute
{
    protected $booleans = [
        "is_filterable",
        "is_filterable_in_grid",
        "is_filterable_in_search",
        "is_html_allowed_on_front",
        "is_required",
        "is_used_in_grid",
        "is_user_defined",
        "is_visible",
        "is_global",
        "is_visible_in_grid",
        "is_wysiwyg_enabled",
        "used_for_sort_by",
    ];

    protected $storeId = 0;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        Data $helper,
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->helper             = $helper;
    }

    /**
     * @param $storeId
     * @param $attribute
     * @return mixed
     */
    public function extractAttributeData($storeId, $attribute)
    {
        $this->setStoreId($storeId);
        $data = $attribute->getData();
        unset ($data['entity_type']);
        $data['id']                     = $data['attribute_id'];
        $data['frontend_labels']        = [];
        $data['default_frontend_label'] = $data['frontend_label'];

        if ($attribute->getFrontendLabels()) {
            foreach ($attribute->getFrontendLabels() as $label) {
                if ($label->getStoreId() == $storeId && $label->getLabel()) {
                    $data['frontend_label'] = $label->getLabel();
                }
            }
        }

        $data['options'] = [];

        $read = $this->resourceConnection->getConnection('core_read');

        $sort = [];
        $rows = $read->fetchAll('

SELECT
	*, (@cnt := @cnt + 1) AS sort
FROM
	eav_attribute_option AS a
CROSS JOIN (
		SELECT @cnt := 0) AS dummy
WHERE
	attribute_id = ?
ORDER BY
	sort_order ASC
', [$attribute->getId()]);

        foreach ($rows as $row) {
            $sort[$row['option_id']] = $row['sort'];
        }

        /* @var $attribute \Magento\Eav\Model\Entity\Attribute */
        $attribute->setStoreId($storeId);
        if ($attribute->getOptions()) {

            foreach ($attribute->getOptions() as $option) {
                /* @var $option \Magento\Eav\Model\Entity\Attribute\Option */
                $data['options'][] = [
                    'label'      => $option->getLabel(),
                    'value'      => (int)$option->getValue(),
                    'sort_order' => isset($sort[$option->getValue()]) ? $sort[$option->getValue()] : 0,
                ];
            }
        }

        array_walk_recursive($data, [$this, 'convertBooleans']);
        return $data;
    }

    public function optimizeAttributeOptions($data, $index)
    {
        // Fetch aggregations for attribute - used later in order to optimize number of attributes sent to the index
        try {
            $usedOptions = $this->helper->aggretageAttributeOptions($index, $data['attribute_code']);
        } catch (\Exception $e) {
            return $data;
        }

        $newOptions = [];
        foreach ($data['options'] as $option) {
            // If able to optimize only send used attribute options
            if (in_array($option['value'], $usedOptions)) {
                $newOptions[] = $option;
            }
        }
        $data['options'] = $newOptions;
        return $data;
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->storeId;
    }

    /**
     * @param int $storeId
     */
    public function setStoreId(int $storeId)
    {
        $this->storeId = $storeId;
    }

    /**
     * @param $string
     * @param $key
     */
    protected function convertBooleans(
        &$string,
        $key
    ) {
        if (in_array($key, $this->booleans)) {
            $string = (bool)$string;
        }
    }
}

