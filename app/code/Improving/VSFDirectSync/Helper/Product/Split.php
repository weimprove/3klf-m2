<?php

namespace Improving\VSFDirectSync\Helper\Product;

use Improving\VSFDirectSync\Model\Sync\Product as SyncProduct;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Split
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var SyncProduct
     */
    private $syncProduct;

    private $cache = [];

    public function __construct(
        SyncProduct $syncProduct,
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->syncProduct       = $syncProduct;
    }

    public function getSplitData(&$productData, $product, $storeId, $optionLabels)
    {
        $splitSku = $productData['split_sku'];

        $skus = [];
        if (!isset($this->cache[$splitSku])) {
            $collection = $this->collectionFactory->create();
            $collection->addStoreFilter($storeId);
            $collection->setStoreId($storeId);
            $collection->addFieldToFilter('status', Status::STATUS_ENABLED);
            $collection->addFieldToFilter('split_sku', $splitSku);
            $collection->addAttributeToFilter('is_saleable', ['eq' => 1], 'left');
            $collection->addAttributeToSelect('filter_color');

            foreach ($collection as $product) {
                $skus[] = $product->getSku();
            }
            $skus = $this->syncProduct->filterConfigurableWithoutChildren($skus, $storeId);
            foreach ($collection as $product) {
                if (in_array($product->getSku(), $skus)) {
                    $attribute = 'filter_color';
                    $color     = null;
                    if (isset($optionLabels[$attribute][$product->getData($attribute)])) {
                        $color = $optionLabels[$attribute][$product->getData($attribute)];
                    }

                    $splitData['linked_products'][] = [
                        'id'        => $product->getId(),
                        'thumbnail' => $product->getThumbnail(),
                        'color'     => $color,
                    ];
                }
            }

            $splitData['count']     = $collection->getSize();
            $this->cache[$splitSku] = $splitData;
        }
        if (isset($this->cache[$splitSku])) {
            $splitData                 = $this->cache[$splitSku];
            $productData['split_data'] = $splitData;
        }
    }
}