<?php

namespace Improving\VSFDirectSync\Helper;

use Elasticsearch\ClientBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    protected $categoryCache = [];
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    private $esClient;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var Context
     */
    private $context;

    /**
     * Data constructor.
     * @param CollectionFactory $collectionFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        CategoryRepositoryInterface $categoryRepository,
        StoreManagerInterface $storeManager,
        Context $context
    ) {
        parent::__construct($context);

        $this->esClient           = ClientBuilder::create()->build();
        $this->collectionFactory  = $collectionFactory;
        $this->categoryRepository = $categoryRepository;
        $this->context            = $context;
        $this->storeManager       = $storeManager;
    }

    public function convertBooleans(
        &$string,
        $key,
        $keys
    ) {
        if (in_array($key, $keys)) {
            $string = (bool)$string;
        }
    }

    public function getStoresToIndex()
    {
        $stores = [];
        foreach ($this->storeManager->getStores() as $store) {
            $enabled = $this->scopeConfig->getValue('vsfdirectsync/general/enabled', 'store', $store->getId());
            if (!$enabled) {
                continue;
            }
            $stores[] = $store;
        }

        return $stores;
    }

    public function cleanUpTheRest($indexedIds, $storeId)
    {
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'store', $storeId);
        $store = $this->storeManager->getStore($storeId);

        $code   = $store->getCode();
        $index1 = $index . '_' . $code;

        $esIds = $this->getEsIds($index1, 'product');

        foreach ($esIds as $id) {
            if (!in_array($id, $indexedIds)) {
                try {
                    $this->esClient->delete([
                        'index' => $index1,
                        'type'  => 'product',
                        'id'    => $id,
                    ]);
                    echo 'Remove disabled products from index: ' . $id . PHP_EOL;
                } catch (\Exception $e) {
                }
            }
        }
    }

    public function getEsIds($index, $entityType)
    {
        $params = [
            "scroll"        => "30s",
            "body"          => [
                "query" => [
                    "match_all" => new \stdClass()
                ]
            ],
            'index'         => $index,
            'type'          => $entityType,
            'size'          => 1000,
            'stored_fields' => [],
        ];

        $esIds    = [];
        $response = $this->esClient->search($params);

        while (isset($response['hits']['hits']) && count($response['hits']['hits']) > 0) {
            foreach ($response['hits']['hits'] as $hit) {
                $esIds[$hit['_id']] = (int)$hit['_id'];
            }

            $scroll_id = $response['_scroll_id'];

            $response = $this->esClient->scroll([
                    "scroll_id" => $scroll_id,
                    "scroll"    => "30s"
                ]
            );
        }
        return $esIds;
    }

    public function aggretageAttributeOptions($index, $attribute)
    {
        $params = [
            'index' => $index,
            "body"  => [
                "size" => 0,
                "aggs" => [
                    "buckets" => [
                        "terms" => ['field' => $attribute, 'size' => 1000],
                    ],
                ],
            ],
        ];

        if (isset($response['_shards']['failures'][0])) {
            throw new \Exception('Elasicsearch aggregation failed');
        }
        $response = $this->esClient->search($params);

        $options = [];

        foreach ($response['aggregations']['buckets']['buckets'] as $row) {
            $options[] = $row['key'];

        }
        return $options;
    }

    public function convertNumber(&$string, $key, $keys)
    {
        if (in_array($key, $keys)) {
            if (!in_array($key, ['sku', 'name', 'description', 'measurement_chart']) && strstr($string, ',')) {
                $string = explode(',', $string);
            }
            return;
        }
        if (is_string($string)) {
            if (is_int($string)) {
                $string = (int)$string;
                return;
            }
            if (is_numeric($string)) {
                $string = (float)$string;
            }
        }
    }

    /**
     * @param $categoryId
     * @param $scope
     * @return bool
     */
    public function isCategoryActive($categoryId, $scope)
    {
        if (!isset($this->categoryCache[$scope])) {
            echo 'Build category cache' . PHP_EOL;

            $collection = $this->collectionFactory->create();
            $collection->setStoreId($scope);
            $collection->addAttributeToSort('level', 'desc');
            $collection->addAttributeToFilter('level', ['gt' => 0]);

            $inactiveIds = [];

            $cache = [];
            foreach ($collection as $item) {
                $category = $this->categoryRepository->get($item->getId(), $scope);

                $categoryData['children_data'] = [];
                $categoryData['id']            = (int)$category->getId();
                $categoryData['parent_id']     = (int)$category->getParentId();
                $categoryData['is_active']     = (bool)$category->getIsActive();

                if ($category->getChildren()) {
                    $children = explode(',', $category->getChildren());
                    foreach ($children as $childId) {
                        $categoryData['children_data'][] = $cache[$childId];
                    }
                }

                $cache[$category->getId()] = $categoryData;
            }

            $inactiveIds = $this->findInactives($cache, $inactiveIds);

            foreach ($inactiveIds as $id) {
                unset($cache[$id]);
            }
            $this->categoryCache[$scope] = $cache;
        }

        return isset($this->categoryCache[$scope][$categoryId]);
    }

    protected function findInactives($categories, $inactiveIds, $inactive = false)
    {
        foreach ($categories as $category) {
            if (!$category['is_active'] || $inactive) {
                $inactiveIds   = $this->findInactives($category['children_data'], $inactiveIds, true);
                $inactiveIds[] = $category['id'];
            }
        }
        return $inactiveIds;
    }
}

