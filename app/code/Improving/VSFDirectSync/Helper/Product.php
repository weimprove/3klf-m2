<?php

namespace Improving\VSFDirectSync\Helper;

use Elasticsearch\ClientBuilder;
use Improving\VSFDirectSync\Helper\Product\Split;
use Improving\VSFDirectSync\Model\ResourceModel\AttributeRenderer\CollectionFactory as AttributeRendererCollectionFactory;
use Magento\Catalog\Model\Product\Type\Price;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface;
use Magento\Catalog\Api\ProductLinkRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\ConfigurableProduct\Helper\Product\Options\Loader;
use Magento\ConfigurableProduct\Model\Product\Type\Collection\SalableProcessor;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class Product
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    protected $esClient;
    /**
     * @var StockRegistryProviderInterface
     */
    protected $stockRegistryProvider;
    /**
     * @var ProductAttributeMediaGalleryManagementInterface
     */
    protected $galleryManagement;

    protected $skipAttributes = [
        "attribute_id",
        "color",
        "gift_message_available",
        "has_options",
        "required_options",
        "size",
        "sku",
        "split_sku",
        "tax_class_id",
        "variant_code",
    ];
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;
    /**
     * @var Loader
     */
    protected $optionLoader;
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;
    /**
     * @var LinkManagementInterface
     */
    protected $linkManagement;
    /**
     * @var SalableProcessor
     */
    protected $salableProcessor;
    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $configurable;
    /**
     * @var \Magento\Bundle\Model\Product\Type
     */
    protected $bundle;
    /**
     * @var array
     */
    protected $idsToProcess = [];
    /**
     * @var array
     */
    protected $optionLabels = [];

    protected $attributesLoaded = false;

    protected $categoryPositionsLoaded = false;

    protected $categoryPositions = [];
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;
    /**
     * @var FilterGroup
     */
    protected $filterGroup;
    /**
     * @var SearchCriteriaInterface
     */
    protected $criteria;
    /**
     * @var State
     */
    protected $state;
    /**
     * @var Status
     */
    protected $productStatus;
    /**
     * @var Visibility
     */
    protected $productVisibility;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var ProductLinkRepositoryInterface
     */
    protected $productLinkRepository;
    /**
     * @var Data
     */
    private $data;
    /**
     * @var Data
     */
    private $helper;
    private $attributeCache;
    private $index;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var AttributeRendererCollectionFactory
     */
    private $attributeRendererCollectionFactory;
    private $attributeRenders;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;
    private $attributeSort = [];
    private $catalogPriceRules = [];
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Split
     */
    private $splitHelper;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var Price
     */
    private $priceModel;
    private $scopeId;

    /**
     * Product constructor.
     * @param AttributeRendererCollectionFactory $attributeRendererCollectionFactory
     * @param AttributeRepositoryInterface $attributeRepository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $helper
     * @param FilterBuilder $filterBuilder
     * @param FilterGroup $filterGroup
     * @param LinkManagementInterface $linkManagement
     * @param Loader $optionLoader
     * @param ProductAttributeMediaGalleryManagementInterface $galleryManagement
     * @param ProductLinkRepositoryInterface $productLinkRepository
     * @param ProductRepositoryInterface $productRepository
     * @param ResourceConnection $resourceConnection
     * @param SalableProcessor $salableProcessor
     * @param ManagerInterface $eventManager
     * @param ScopeConfigInterface $scopeConfig
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SearchCriteriaInterface $criteria
     * @param State $state
     * @param Split $splitHelper
     * @param Price $priceModel
     * @param Status $productStatus
     * @param StockRegistryProviderInterface $stockRegistryProvider
     * @param StoreManagerInterface $storeManager
     * @param ProductCollectionFactory $productCollectionFactory
     * @param Visibility $productVisibility
     */
    public function __construct(
        AttributeRendererCollectionFactory $attributeRendererCollectionFactory,
        AttributeRepositoryInterface $attributeRepository,
        CategoryRepositoryInterface $categoryRepository,
        Data $helper,
        FilterBuilder $filterBuilder,
        FilterGroup $filterGroup,
        LinkManagementInterface $linkManagement,
        Loader $optionLoader,
        ProductAttributeMediaGalleryManagementInterface $galleryManagement,
        ProductLinkRepositoryInterface $productLinkRepository,
        ProductRepositoryInterface $productRepository,
        ResourceConnection $resourceConnection,
        SalableProcessor $salableProcessor,
        ManagerInterface $eventManager,
        ScopeConfigInterface $scopeConfig,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SearchCriteriaInterface $criteria,
        State $state,
        Split $splitHelper,
        Price $priceModel,
        Status $productStatus,
        StockRegistryProviderInterface $stockRegistryProvider,
        StoreManagerInterface $storeManager,
        ProductCollectionFactory $productCollectionFactory,
        Visibility $productVisibility
    ) {
        $this->esClient = ClientBuilder::create()->build();

        $this->attributeRendererCollectionFactory = $attributeRendererCollectionFactory;
        $this->attributeRepository                = $attributeRepository;
        $this->categoryRepository                 = $categoryRepository;
        $this->criteria                           = $criteria;
        $this->filterBuilder                      = $filterBuilder;
        $this->filterGroup                        = $filterGroup;
        $this->galleryManagement                  = $galleryManagement;
        $this->helper                             = $helper;
        $this->linkManagement                     = $linkManagement;
        $this->optionLoader                       = $optionLoader;
        $this->productLinkRepository              = $productLinkRepository;
        $this->productRepository                  = $productRepository;
        $this->productStatus                      = $productStatus;
        $this->productVisibility                  = $productVisibility;
        $this->resourceConnection                 = $resourceConnection;
        $this->salableProcessor                   = $salableProcessor;
        $this->scopeConfig                        = $scopeConfig;
        $this->searchCriteriaBuilder              = $searchCriteriaBuilder;
        $this->state                              = $state;
        $this->stockRegistryProvider              = $stockRegistryProvider;
        $this->storeManager                       = $storeManager;
        $this->splitHelper                        = $splitHelper;
        $this->eventManager                       = $eventManager;
        $this->productCollectionFactory           = $productCollectionFactory;
        $this->priceModel                         = $priceModel;
    }

    public function indexBatch($skus, $scopeId = 1, $index = false)
    {
        $this->index = $index;
        $params      = [
            'body' => []
        ];

        $this->scopeId = $scopeId;

        $indexedIds = [];
        $deleteIds  = [];
        $chunks     = array_chunk($skus, 500);

        printf('Number of chunks to index: %s', count($chunks));
        echo PHP_EOL;

        foreach ($chunks as $key => $chunk) {
            printf('Processing no: %s', $key + 1);
            foreach ($chunk as $sku) {
                echo '.';
                $productData = $this->extractData($sku);

                if ($productData['status'] == 2) {
                    $deleteIds[] = $productData['id'];
                    continue;
                }
                $params['body'][] = [
                    'index' => [
                        '_index' => $this->index,
                        '_type'  => 'product',
                        '_id'    => (int)$productData['id']
                    ]
                ];

                $params['body'][] = $productData;
                $indexedIds[]     = (int)$productData['id'];
            }
            echo PHP_EOL;

            printf('Send chunk to ES');
            echo PHP_EOL;
            if (!empty($params['body'])) {
                $responses = $this->esClient->bulk($params);
                if (isset($responses['errors']) && $responses['errors']) {
                    foreach ($responses['items'] as $item) {
                        if (isset($item['index']['error'])) {
                            echo 'Error on document id: ' . $item['index']['_id'] . PHP_EOL;
                            print_r($item['index']['error']);
                        }
                    }
                }
            }
        }
        if (!empty($deleteIds)) {
            foreach ($deleteIds as $id) {

                try {
                    $this->esClient->delete([
                        'index' => $this->index,
                        'type'  => 'product',
                        'id'    => (string)$id,
                    ]);
                    echo 'DELETE FROM index: ' . $id . PHP_EOL;
                } catch (\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                    // Fail silently
                }
            }
        }
        return $indexedIds;
    }

    public function extractData(
        $sku
    ) {
        $this->loadAttributeData();
        $this->loadCategoryPositions();

        try {
            $product = $this->productRepository->get($sku, false, $this->scopeId);
        } catch (\Exception $e) {
            return false;
        }

        $stockItem   = $this->stockRegistryProvider->getStockItem($product->getId(), $this->scopeId);
        $productData = $product->convertToArray();

        if (!isset($productData['special_to_date'])) {
            $productData['special_to_date'] = date('Y-m-d H:i:s', strtotime('+3 days'));
        }
        if (!isset($productData['special_from_date'])) {
            $productData['special_from_date'] = date('Y-m-d H:i:s', strtotime('-1 day'));
        }

        $productData['stock'] = $this->parseStockData($stockItem->getData());
        $productData['id']    = $product->getId();

        $productData['category']     = [];
        $productData['category_ids'] = [];

        foreach ($product->getCategoryIds() as $categoryId) {
            $i        = count($productData['category_ids']);
            $category = $this->categoryRepository->get($categoryId, $this->scopeId);

            $productData['category_ids'][$i] = (int)$categoryId;

            $position = 999;
            if (isset($this->categoryPositions[$product->getId()]) && isset($this->categoryPositions[$product->getId()][$categoryId])) {
                $position = $this->categoryPositions[$product->getId()][$categoryId];

            }
            if ($this->helper->isCategoryActive($category->getId(), $this->scopeId)) {
                $productData['category_ids'][$i] = (int)$categoryId;

                $productData['category'][] = [
                    'category_id' => $categoryId,
                    'name'        => $category->getName(),
                    'position'    => $position
                ];
            }
        }

        $productData['tier_prices'] = [];
        $tierPrices                 = $product->getTierPrices();
        foreach ($tierPrices as $tierPrice) {
            $productData['tier_prices'][] = $tierPrice->getData();
        }

        $productData['custom_attributes'] = null;

        $productData['product_links'] = [];

        $list = $product->getProductLinks();
        foreach ($list as $item) {
            $productData['product_links'][] = $item->getData();
        }

        $gallery = $product->getMediaGallery();

        $productData['media_gallery'] = [];

        $files = [];

        if ($gallery) {
            foreach ($gallery['images'] as $galleryEntry) {
                $files[] = $galleryEntry['file'];

                $productData['media_gallery'][] = [
                    'image' => $galleryEntry['file'],
                    'pos'   => $galleryEntry['position'],
                    'typ'   => $galleryEntry['media_type'],
                    'lab'   => $galleryEntry['label'] . ' ',
                ];
            }
        }

        if (!empty($productData['media_gallery']) && (!isset($productData['small_image']) || !in_array($productData['small_image'], $files))) {
            $productData['small_image'] = $productData['media_gallery'][0]['image'];
        }
        if (!empty($productData['media_gallery']) && (!isset($productData['image']) || !in_array($productData['image'], $files))) {
            $productData['image'] = $productData['media_gallery'][0]['image'];
        }
        if (!empty($productData['media_gallery']) && (!isset($productData['thumbnail']) || !in_array($productData['thumbnail'], $files))) {
            $productData['thumbnail'] = $productData['media_gallery'][0]['image'];
        }

        if ($product->getTypeId() == 'bundle') {
            $optionCollection = $product->getTypeInstance()->getOptionsCollection($product);
            foreach ($optionCollection as $item) {
                $selectionCollection = $product->getTypeInstance()->getSelectionsCollection([$item->getOptionId()], $product);

                $option                  = $item->getData();
                $option['sku']           = $product->getSku();
                $option['product_links'] = [];

                foreach ($selectionCollection as $selection) {
                    if ($selection->getStatus() == Status::STATUS_DISABLED) {
                        continue;
                    }

                    $selectionPriceType = $product->getPriceType() ? $selection->getSelectionPriceType() : null;
                    $selectionPrice     = $product->getPriceType() ? $selection->getSelectionPriceValue() : null;

                    $option['product_links'][] = [
                        'price'               => $selectionPrice,
                        'qty'                 => $selection->getSelectionQty(),
                        'price_type'          => $selectionPriceType,
                        'can_change_quantity' => $selection->getSelectionCanChangeQty(),
                        'option_id'           => $item->getOptionId(),
                        'id'                  => $selection->getSelectionId(),
                        'position'            => $selection->getPosition(),
                        'sku'                 => $selection->getSku(),
                        'is_default'          => $selection->getIsDefault(),
                    ];
                }
                $productData['bundle_options'][] = $option;
            }

            // Bail if there are no options
            if (!isset($productData['bundle_options']) || !count($productData['bundle_options'])) {
                return;
            }
        }
        if ($product->getTypeId() == 'simple') {
            $productData['final_price'] = $productData['price'];
            $productData['is_new']      = true;
            if (isset($productData['special_price']) && $productData['special_price'] && $productData['special_price'] != $productData['price']) {
                $productData['is_new']      = false;
                $productData['final_price'] = $productData['special_price'];
            } else {
                unset($productData['special_price']);
            }
        }

        if ($product->getTypeId() == 'configurable') {
            $type            = $product->getTypeInstance();
            $childCollection = $type->getUsedProductCollection($product);
            $childCollection->addAttributeToSelect('*');
            $keepOptions = [];

            foreach ($childCollection as $childProduct) {
                $keepOptions['size'][]   = $childProduct->getSize();
                $keepOptions['length'][] = $childProduct->getLength();
                $keepOptions['color'][]  = $childProduct->getColor();

                $childData = $childProduct->convertToArray();
                unset($childData['split_sku']);

                $childData['id']     = $childProduct->getId();
                $childData['status'] = $childProduct->getStatus();

                $childData['price']         = $childProduct->getPrice();
                $childData['final_price']   = $childProduct->getFinalPrice();
                $childData['special_price'] = $childProduct->getSpecialPrice();

                $priceRule = $this->getCatalogPriceRule($childProduct);
                if ($priceRule) {
                    $childData['final_price']   = $priceRule['final_price'];
                    $childData['special_price'] = $priceRule['special_price'];
                    unset($childData['special_from_date']);
                    unset($childData['special_to_date']);
                }

                $tierPrice = $this->priceModel->getTierPrice(1, $childProduct);
                if ($tierPrice && $tierPrice < $childData['final_price']) {
                    $childData['final_price']   = $tierPrice;
                    $childData['special_price'] = $tierPrice;
                    unset($childData['special_from_date']);
                    unset($childData['special_to_date']);
                }

                if (!isset($childData['special_to_date'])) {
                    $childData['special_to_date'] = date('Y-m-d H:i:s', strtotime('+3 days'));
                }
                if (!isset($childData['special_from_date'])) {
                    $childData['special_from_date'] = date('Y-m-d H:i:s', strtotime('-1 day'));
                }

                $childData['tier_prices'] = [];
                $tierPrices               = $childProduct->getTierPrices();
                foreach ($tierPrices as $tierPrice) {
                    $childData['tier_prices'][] = $tierPrice->getData();
                }

                $childData['category_ids'] = [];
                foreach ($childProduct->getCategoryIds() as $categoryId) {
                    $i                             = count($childData['category_ids']);
                    $childData['category_ids'][$i] = (int)$categoryId;
                }

                unset($childData['entity_id']);
                unset($childData['store_id']);
                unset($childData['updated_at']);
                unset($childData['created_at']);
                unset($childData['weight']);
                unset($childData['type_id']);
                unset($childData['attribute_set_id']);

                foreach ($childData as $key => $value) {
                    if ($value == 'no_selection') {
                        unset($childData[$key]);
                    }
                }

                $childData['media_gallery'] = [];

                $gallery = $childProduct->getMediaGallery();
                if ($gallery) {
                    foreach ($gallery['images'] as $galleryEntry) {
                        $childData['media_gallery'][] = [
                            'image' => $galleryEntry['file'],
                            'pos'   => $galleryEntry['position'],
                            'typ'   => $galleryEntry['media_type'],
                            'lab'   => $galleryEntry['label'] . ' ',
                        ];
                    }
                }

                foreach ($childData as $key => $value) {
                    if ($value == 'no_selection') {
                        unset($childData[$key]);
                    }
                }

                $productData['configurable_children'][] = $childData;

                $productData['price']       = $childData['price'];
                $productData['final_price'] = $childData['price'];
                $productData['is_new']      = true;
                if (isset($childData['special_price']) && $childData['special_price'] != $childData['price']) {
                    $productData['is_new']        = false;
                    $productData['special_price'] = $childData['special_price'];
                    $productData['final_price']   = $childData['special_price'];
                }
                $childProduct->clearInstance();
            }

            if (count($keepOptions)) {
                $configurableOptions = (array)$this->optionLoader->load($product);
                foreach ($configurableOptions as $option) {
                    $attribute                    = $this->getAttribute('catalog_product', $option->getAttributeId());
                    $attributeCode                = $attribute->getAttributeCode();
                    $optionData['attribute_id']   = $option->getAttributeId();
                    $optionData['attribute_code'] = $attributeCode;
                    $optionData['product_id']     = $option->getProductId();
                    $optionData['id']             = $option->getId();
                    $optionData['position']       = (int)$option->getPosition();
                    $optionData['label']          = str_replace('Product ', '', $option->getLabel());
                    $optionData['values']         = [];
                    $values                       = [];

                    foreach ($option->getValues() as $value) {
                        if (!isset($keepOptions[$attributeCode])) {
                            continue;
                        }

                        if (!in_array((int)$value->getValueIndex(), $keepOptions[$attributeCode])) {
                            continue;
                        }
                        $sort = 1;
                        if (isset($this->attributeSort[$attributeCode][$value->getValueIndex()])) {
                            $sort = $this->attributeSort[$attributeCode][$value->getValueIndex()];
                        } else {
                            $sort++;
                        }

                        $values[$sort]['value_index'] = $value->getValueIndex();
                        $values[$sort]['label']       = null;

                        if (isset($this->optionLabels[$attributeCode][$value->getValueIndex()])) {
                            $values[$sort]['label'] = $this->optionLabels[$attributeCode][$value->getValueIndex()];
                        }
                        if (!isset($productData[$attributeCode . '_options'])) {
                            $productData[$attributeCode . '_options'] = [];
                            $productData[$attributeCode]              = [];
                        }
                        $i                                            = count($productData[$attributeCode . '_options']);
                        $productData[$attributeCode . '_options'][$i] = (int)$value->getValueIndex();
                        $productData[$attributeCode][$i]              = (int)$value->getValueIndex();
                    }

                    ksort($values);
                    $optionData['values'] = array_values($values);

                    if (!count($values)) {
                        continue;
                    }

                    $productData['configurable_options'][] = $optionData;
                }
            }
        }
        if (in_array($product->getVisibility(), $product->getVisibleInSiteVisibilities())) {
            $productData['url_path'] = $product->getUrlKey();
        }

        unset($productData['extension_attributes']);
        unset($productData['quantity_and_stock_status']);
        unset($productData['is_salable']);
        unset($productData['options']);
        unset($productData['entity_id']);
        $productData['tsk'] = time();

        array_walk_recursive($productData, [$this->helper, 'convertNumber'], $this->skipAttributes);

        foreach ($productData as $key => $value) {
            $value = is_array($value) ? $value : [$value];
            if (isset($this->attributeRenders[$key])) {
                $rendered                      = $this->attributeRenders[$key]->render($value);
                $productData['rendered'][$key] = $rendered;
            }
        }

        if (isset($productData['split_sku']) && $productData['split_sku']) {
            $this->splitHelper->getSplitData($productData, $product, $this->scopeId, $this->optionLabels);
        }

        $productData['brand_data'] = $this->getBrandData($product);

        $productData['sku'] = (string)$productData['sku'];
        $product->clearInstance();

        return $productData;
    }

    protected function loadAttributeData()
    {
        if (!($this->attributesLoaded)) {
            $read           = $this->resourceConnection->getConnection('core_read');
            $searchCriteria = $this->searchCriteriaBuilder
                ->create();

            // Load attribute option sorting
            foreach ($this->attributeRepository->getList('catalog_product', $searchCriteria)->getItems() as $item) {
                $sql  = <<<'SQL'
SELECT
	*, (@cnt := @cnt + 1) AS sort
FROM
	eav_attribute_option AS a
CROSS JOIN (
		SELECT @cnt := 0) AS dummy
WHERE
	attribute_id = ?
ORDER BY
	sort_order ASC
SQL;
                $rows = $read->fetchAll($sql, [$item->getAttributeId()]);
                foreach ($rows as $row) {
                    $this->attributeSort[$item->getAttributeCode()][$row['option_id']] = $row['sort'];
                }
                $attrCode = $item->getAttributeCode();

                $attribute = $this->getAttribute('catalog_product', $attrCode);
                $options   = $attribute->getSource()->getAllOptions();

                foreach ($options as $option) {
                    $this->optionLabels[$attrCode][$option['value']] = $option['label'];
                }

                $this->skipAttributes[] = $item->getAttributeCode();
            }

            // Load Attribute renders
            $attributeRenders = $this->attributeRendererCollectionFactory->create();
            foreach ($attributeRenders as $renderer) {
                if ($renderer->getEntityTypeId() == 4) {
                    $this->attributeRenders[$renderer->getAttributeCode()] = $renderer->getRendererObject();
                }
            }

            $this->attributesLoaded = true;
        }
    }

    protected function getAttribute($entityType, $attributeId)
    {
        if (!isset($this->attributeCache[$entityType]) || !isset($this->attributeCache[$entityType][$attributeId])) {
            $this->attributeCache[$entityType][$attributeId] = $this->attributeRepository->get('catalog_product', $attributeId);
            return $this->attributeCache[$entityType][$attributeId];
        }
        return $this->attributeCache[$entityType][$attributeId];
    }

    private function loadCategoryPositions()
    {
        if (!$this->categoryPositionsLoaded) {
            $read = $this->resourceConnection->getConnection('core_read');

            $rows = $read->fetchAll('SELECT *  FROM catalog_category_product');
            foreach ($rows as $row) {
                $this->categoryPositions[$row['product_id']][$row['category_id']] = $row['position'];
            }
        }
        $this->categoryPositionsLoaded = true;
    }

    protected function parseStockData($stockItemData)
    {
        $booleans = [
            "enable_qty_increments",
            "is_decimal_divided",
            "is_in_stock",
            "is_qty_decimal",
            "manage_stock",
            "show_default_notification_message",
            "use_config_backorders",
            "use_config_enable_qty_inc",
            "use_config_manage_stock",
            "use_config_max_sale_qty",
            "use_config_min_qty",
            "use_config_notify_stock_qty",
            "use_config_qty_increments",
        ];
        foreach ($stockItemData as $key => &$value) {
            if (in_array($key, $booleans)) {
                $value = (bool)$value;
            }
        }

        return $stockItemData;
    }

    protected function getCatalogPriceRule($childProduct)
    {
        if (!$this->catalogPriceRules) {
            $read = $this->resourceConnection->getConnection('core_read');

            $sql  = <<<'SQL'
            SELECT * FROM catalogrule_product_price
SQL;
            $rows = $read->fetchAll($sql);
            $data = [];

            foreach ($rows as $row) {
                if (!isset($data[$row['website_id']])) {
                    $data[$row['website_id']] = [];
                }
                $data[$row['website_id']][$row['product_id']] = $row;
            }

            $this->catalogPriceRules = $data;
        }
        $data     = $this->catalogPriceRules;
        $ruleData = [];

        $store     = $this->storeManager->getStore($this->scopeId);
        $websiteId = $store->getWebsiteId();

        if (isset($data[$websiteId]) && isset($data[$websiteId][$childProduct->getId()])) {
            $item     = $data[$websiteId][$childProduct->getId()];
            $ruleData = [
                'final_price'   => $item['rule_price'],
                'special_price' => $item['rule_price'],
            ];
        }

        return $ruleData;
    }

    public function getBrandData($product)
    {
        if (isset($this->optionLabels['brand'][$product->getBrand()])) {
            $brandName = $this->optionLabels['brand'][$product->getBrand()];
            return [
                'name' => $brandName,
            ];
        }
    }

    public function getScopeId()
    {
        return $this->scopeId;
    }
}
