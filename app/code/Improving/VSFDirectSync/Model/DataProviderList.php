<?php

namespace Improving\VSFDirectSync\Model;

class DataProviderList
{
    /**
     * @var string[]
     */
    protected $providers;

    /**
     * Constructor
     *
     * @param array $providers
     */
    public function __construct(array $providers = [])
    {
        $this->providers = $providers;
    }

    /**
     * {@inheritdoc}
     */
    public function getProviders()
    {
        return $this->providers;
    }
}
