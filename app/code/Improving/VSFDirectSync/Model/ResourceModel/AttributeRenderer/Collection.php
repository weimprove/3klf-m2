<?php

namespace Improving\VSFDirectSync\Model\ResourceModel\AttributeRenderer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'vsfdirectsync_attribute_renderer_collection';
    protected $_eventObject = 'attribute_renderer_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Improving\VSFDirectSync\Model\AttributeRenderer', 'Improving\VSFDirectSync\Model\ResourceModel\AttributeRenderer');
    }

}
