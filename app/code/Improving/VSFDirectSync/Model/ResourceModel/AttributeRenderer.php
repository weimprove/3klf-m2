<?php

namespace Improving\VSFDirectSync\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class AttributeRenderer extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('vsfdirectsync_attribute_renderer', 'id');
    }

}
