<?php

namespace Improving\VSFDirectSync\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\ObjectManagerInterface;

class AttributeRenderer extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'vsfdirectsync_attribute_renderer';
    protected $_cacheTag = 'vsfdirectsync_attribute_renderer';
    protected $_eventPrefix = 'vsfdirectsync_attribute_renderer';
    /**
     * @var Context
     */
    private $context;
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;
    /**
     * @var ResourceModel\AbstractResource
     */
    private $resource;
    /**
     * @var \Magento\Framework\Data\Collection\AbstractDb
     */
    private $resourceCollection;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var array
     */
    private $data;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->context = $context;
        $this->registry = $registry;
        $this->resource = $resource;
        $this->resourceCollection = $resourceCollection;
        $this->objectManager = $objectManager;
        $this->data = $data;
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function getRendererObject()
    {
        return $this->objectManager->create($this->getRenderer());
    }

    protected function _construct()
    {
        $this->_init('Improving\VSFDirectSync\Model\ResourceModel\AttributeRenderer');
    }
}
