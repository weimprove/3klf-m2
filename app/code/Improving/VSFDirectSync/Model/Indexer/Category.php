<?php

namespace Improving\VSFDirectSync\Model\Indexer;

use Improving\VSFDirectSync\Model\Sync\Category as SyncCategory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Api\GroupRepositoryInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;

class Category implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{
    /**
     * @var GroupRepositoryInterface
     */
    protected $groupRepository;
    /**
     * @var SyncCategory
     */
    private $category;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        SyncCategory $category,
        Emulation $emulation,
        StoreManagerInterface $storeManager,
        GroupRepositoryInterface $groupRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->category        = $category;
        $this->emulation       = $emulation;
        $this->scopeConfig     = $scopeConfig;
        $this->storeManager    = $storeManager;
        $this->groupRepository = $groupRepository;
    }

    public function executeFull()
    {
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'websites');
        foreach ($this->storeManager->getStores() as $store) {
            $id   = $store->getId();
            $code = $store->getCode();

            $this->emulation->startEnvironmentEmulation($id);
            $this->index = $index . '_' . $code;

            $group = $this->groupRepository->get($store->getStoreGroupId());
            $this->category->indexForStore($id, $group->getRootCategoryId(), $this->index);
        }
    }

    public function executeList(array $ids)
    {
        $this->execute($ids);
    }

    public function execute($ids)
    {
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'websites');
        foreach ($this->storeManager->getStores() as $store) {
            $storeId = $store->getId();
            $code    = $store->getCode();

            $this->emulation->startEnvironmentEmulation($storeId);
            $storeIndex = $index . '_' . $code;
            $this->category->indexIdsForStore($ids, $storeId, $storeIndex);
        }
    }

    public function executeRow($id)
    {
        $this->execute([$id]);
    }
}
