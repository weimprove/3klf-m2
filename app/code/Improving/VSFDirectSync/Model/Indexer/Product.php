<?php

namespace Improving\VSFDirectSync\Model\Indexer;

use Improving\VSFDirectSync\Helper\Data;
use Improving\VSFDirectSync\Model\Indexer\Product\DataProvider;
use Improving\VSFDirectSync\Model\Indexer\Product\Resolver;
use Improving\VSFDirectSync\Model\Indexer\Product\Saver;

class Product implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{
    /**
     * @var DataProvider
     */
    protected $dataProvider;
    /**
     * @var Saver
     */
    protected $saver;
    /**
     * @var Resolver
     */
    protected $resolver;
    /**
     * @var Data
     */
    private $helper;

    /**
     * Product constructor.
     * @param DataProvider $dataProvider
     * @param Saver $saver
     * @param Resolver $resolver
     */
    public function __construct(
        DataProvider $dataProvider,
        Saver $saver,
        Resolver $resolver
    ) {
        $this->dataProvider = $dataProvider;
        $this->saver        = $saver;
        $this->resolver     = $resolver;
    }

    public function executeFull()
    {
        foreach ($this->helper->getStoresToIndex() as $store) {
            $ids = $this->resolver->getProductIds($store->getWebsiteId(), $store->getStoreId());
            $this->dataProvider->rebuildStoreIndex($store->getWebsiteId(), $store->getStoreId(), $ids, $this->saver);
        }
    }

    public function executeList(array $ids)
    {
        $this->execute($ids);
    }

    public function execute($ids)
    {
        foreach ($this->helper->getStoresToIndex() as $store) {
            $this->dataProvider->rebuildStoreIndex($store->getWebsiteId(), $store->getStoreId(), $ids, $this->saver);
        }
    }

    public function executeRow($id)
    {
        $this->execute([$id]);
    }
}
