<?php

namespace Improving\VSFDirectSync\Model\Indexer\Product;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Resolver
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var Visibility
     */
    protected $productVisibility;

    /**
     * Resolver constructor.
     * @param CollectionFactory $collectionFactory
     * @param Visibility $productVisibility
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Visibility $productVisibility
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->productVisibility = $productVisibility;
    }

    public function getProductIds($websiteId, $storeId)
    {
        $collection = $this->collectionFactory->create();
        $collection->addWebsiteFilter($websiteId);
        $collection->addStoreFilter($storeId);
        $collection->addFieldToFilter('status', Status::STATUS_ENABLED);
        $collection->addAttributeToFilter('visibility', $this->productVisibility->getVisibleInSiteIds());

        return $collection->getAllIds();
    }
}
