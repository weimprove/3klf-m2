<?php

namespace Improving\VSFDirectSync\Model\Indexer\Product;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;

class Saver
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var Client
     */
    private $esClient;

    /*
     *
     */
    private $indexedIds = [];

    /**
     * Saver constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->esClient     = ClientBuilder::create()->build();
        $this->scopeConfig  = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    public function put($chunk, $storeId)
    {
        $index = $this->scopeConfig->getValue('vsfdirectsync/general/index', 'store', $storeId);
        $store = $this->storeManager->getStore($storeId);

        $code   = $store->getCode();
        $index1 = $index . '_' . $code;

        foreach ($chunk as $productData) {
            if ($productData['status'] == 2) {
                try {
                    $this->esClient->delete([
                        'index' => $index1,
                        'type'  => 'product',
                        'id'    => (string)$productData['entity_id'],
                    ]);
                } catch (\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                    // Fail silently
                }
                continue;
            }

            $params['body'][] = [
                'index' => [
                    '_index' => $index1,
                    '_type'  => 'product',
                    '_id'    => (int)$productData['entity_id']
                ]
            ];

            $params['body'][] = $productData;

            $this->indexedIds[$storeId][] = (int)$productData['entity_id'];
        }

        if (!empty($params['body'])) {
            $responses = $this->esClient->bulk($params);
            if (isset($responses['errors']) && $responses['errors']) {
                foreach ($responses['items'] as $item) {
                    if (isset($item['index']['error'])) {
                        // Log error
                        error_log(print_r($item['index']['error']), 1);
                    }
                }
            }
        }
    }

    public function getIndexedIds(int $storeId): array
    {
        return $this->indexedIds[$storeId] ?? [];
    }

}
