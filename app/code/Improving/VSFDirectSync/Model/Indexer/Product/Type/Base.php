<?php

namespace Improving\VSFDirectSync\Model\Indexer\Product\Type;

use Improving\VSFDirectSync\Model\Sync\Product as SyncProduct;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use \Magento\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider as FullTextDataProvider;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Model\Config;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;
use Magento\Store\Model\Store;

class Base
{
    /**
     * @var CollectionFactory
     */
    protected $prodAttributeCollectionFactory;
    /**
     * @var FullTextDataProvider
     */
    protected $dataProvider;
    /**
     * @var Config
     */
    protected $eavConfig;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;
    protected $scopeId;
    /**
     * @var SyncProduct
     */
    protected $syncProduct;
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;
    /**
     * @var DataProviderList
     */
    protected $dataProviderList;
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    private $attributes;
    private $attributesByBackendType = [];

    /**
     * DataProvider constructor.
     * @param CollectionFactory $prodAttributeCollectionFactory
     * @param ResourceConnection $resourceConnection
     * @param Config $eavConfig
     * @param AttributeRepositoryInterface $attributeRepository
     * @param FullTextDataProvider $dataProvider
     */
    public function __construct(
        CollectionFactory $prodAttributeCollectionFactory,
        ResourceConnection $resourceConnection,
        Config $eavConfig,
        AttributeRepositoryInterface $attributeRepository,
        FullTextDataProvider $dataProvider,
        SyncProduct $syncProduct,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->prodAttributeCollectionFactory = $prodAttributeCollectionFactory;
        $this->dataProvider                   = $dataProvider;
        $this->eavConfig                      = $eavConfig;
        $this->resourceConnection             = $resourceConnection;
        $this->resource                       = $resourceConnection;
        $this->connection                     = $this->resourceConnection->getConnection();
        $this->attributeRepository            = $attributeRepository;
        $this->syncProduct                    = $syncProduct;
        $this->productCollectionFactory       = $productCollectionFactory;
    }

    public function getAttributes($backendType = null)
    {
        if (null === $this->attributes) {
            $productAttributes = $this->prodAttributeCollectionFactory->create();
            $attributes        = $productAttributes->getItems();
            $entity            = $this->eavConfig->getEntityType(\Magento\Catalog\Model\Product::ENTITY)->getEntity();

            foreach ($attributes as $attribute) {
                $attribute->setEntity($entity);
                $this->attributes[$attribute->getAttributeId()]   = $attribute;
                $this->attributes[$attribute->getAttributeCode()] = $attribute;
            }
        }

        if ($backendType !== null) {
            if (isset($this->attributesByBackendType[$backendType])) {
                return $this->attributesByBackendType[$backendType];
            }
            $this->attributesByBackendType[$backendType] = [];

            foreach ($this->attributes as $attribute) {
                if ($attribute->getBackendType() == $backendType) {
                    $this->attributesByBackendType[$backendType][$attribute->getAttributeId()] = $attribute;
                }
            }

            return $this->attributesByBackendType[$backendType];
        }
    }

    public function beforeAddToChunk($productData)
    {
        return $productData;
    }

    public function getProducts($productIds, $websiteId, $storeId, $staticFields, $pageSize, $lastProductId = 0)
    {
        $select = $this->connection->select()
            ->useStraightJoin(true)
            ->from(
                ['e' => $this->getTable('catalog_product_entity')],
                array_merge(['entity_id', 'type_id'], $staticFields)
            )
            ->join(
                ['website' => $this->getTable('catalog_product_website')],
                $this->connection->quoteInto('website.product_id = e.entity_id AND website.website_id = ?', $websiteId),
                []
            );

        if ($productIds !== null) {
            $select->where('e.entity_id IN (?)', $productIds);
        }
        $select->where('e.entity_id > ?', $lastProductId ? $lastProductId + 1 : 0);
        $select->order('e.entity_id');
        $select->limit($pageSize);

        return $this->connection->fetchAll($select);
    }

    /**
     * Return validated table name
     *
     * @param string|string[] $table
     * @return string
     */
    protected function getTable($table)
    {
        return $this->resource->getTableName($table);
    }

    public function getCategories($productIds, $websiteId, $storeId)
    {
        $select = $this->connection->select()
            ->useStraightJoin(true)
            ->from(
                ['ccp' => $this->getTable('catalog_category_product')]
            )
            ->join(
                ['e' => $this->getTable('catalog_category_entity')],
                'e.entity_id = ccp.category_id',
                []
            )
            ->join(
                ['sg' => $this->getTable('store_group')],
                $this->connection->quoteInto('e.path LIKE CONCAT("%/", sg.root_category_id, "/%") AND website_id = ?', [$websiteId]),
                []
            );
        $this->joinAttribute($select, 'name', $storeId, [], 'catalog_category');
        $select->where('ccp.product_id IN (?)', $productIds)->columns(['IF (name_store.value IS NULL, name_default.value, name_store.value) as name']);

        $rows = $this->connection->fetchAll($select);

        $result = [];
        foreach ($rows as $row) {
            $result[$row['product_id']]['category'][]     = [
                'category_id' => $row['category_id'],
                'name'        => $row['name'],
                'position'    => $row['position'],
            ];
            $result[$row['product_id']]['category_ids'][] = $row['category_id'];
        }
        return $result;
    }

    /**
     * Join attribute to searchable product for filtration
     *
     * @param Select $select
     * @param string $attributeCode
     * @param int $storeId
     * @param array $whereValue
     * @param string $entityTypeCode
     * @param string $join
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function joinAttribute(
        Select $select,
        $attributeCode,
        $storeId,
        array $whereValue,
        $entityTypeCode = 'catalog_product',
        $join = 'join'
    ) {
        $linkField      = 'entity_id';
        $attribute      = $this->attributeRepository->get($entityTypeCode, $attributeCode);
        $attributeTable = $this->getTable($entityTypeCode . '_entity_' . $attribute->getBackendType());
        $defaultAlias   = $attributeCode . '_default';
        $storeAlias     = $attributeCode . '_store';

        $whereCondition = $this->connection->getCheckSql(
            $storeAlias . '.value_id > 0',
            $storeAlias . '.value',
            $defaultAlias . '.value'
        );

        $select->$join(
            [$defaultAlias => $attributeTable],
            $this->connection->quoteInto(
                $defaultAlias . '.' . $linkField . '= e.' . $linkField . ' AND ' . $defaultAlias . '.attribute_id = ?',
                $attribute->getAttributeId()
            ) . $this->connection->quoteInto(
                ' AND ' . $defaultAlias . '.store_id = ?',
                Store::DEFAULT_STORE_ID
            ),
            []
        )->joinLeft(
            [$storeAlias => $attributeTable],
            $this->connection->quoteInto(
                $storeAlias . '.' . $linkField . '= e.' . $linkField . ' AND ' . $storeAlias . '.attribute_id = ?',
                $attribute->getAttributeId()
            ) . $this->connection->quoteInto(
                ' AND ' . $storeAlias . '.store_id = ?',
                $storeId
            ),
            []
        );
        if (count($whereValue)) {
            $select->where(
                $whereCondition . ' IN (?)',
                $whereValue
            );
        }
    }

    /**
     * @param $productIds
     * @param $storeId
     * @return array
     */
    public function getConfigurableOptions($productIds, $storeId): array
    {
        $select = $this->connection->select()
            ->useStraightJoin(true)
            ->from(
                ['cpsl' => $this->getTable('catalog_product_super_link')]
            )
            ->join(
                ['cpsa' => $this->getTable('catalog_product_super_attribute')],
                'cpsl.parent_id = cpsa.product_id',
                []
            )
            ->join(
                ['ea' => $this->getTable('eav_attribute')],
                'cpsa.attribute_id = ea.attribute_id',
                []
            )
            ->join(
                ['cpei' => $this->getTable('catalog_product_entity_int')],
                'cpsa.attribute_id = cpei.attribute_id AND cpei.entity_id = cpsl.product_id',
                []
            )
            ->join(
                ['option' => $this->getTable('eav_attribute_option')],
                'cpei.value = option.option_id',
                []
            )
            ->join(
                ['attr_default' => $this->getTable('eav_attribute_label')],
                $this->connection->quoteInto('attr_default.attribute_id = cpsa.attribute_id AND attr_default.store_id = ?', [0]),
                []
            )
            ->joinLeft(
                ['attr_store' => $this->getTable('eav_attribute_label')],
                $this->connection->quoteInto('attr_store.attribute_id = cpsa.attribute_id AND attr_store.store_id = ?', [$storeId]),
                []
            )
            ->join(
                ['label_default' => $this->getTable('eav_attribute_option_value')],
                $this->connection->quoteInto('label_default.option_id = cpei.value AND label_default.store_id = ?', [0]),
                []
            )
            ->joinLeft(
                ['label_store' => $this->getTable('eav_attribute_option_value')],
                $this->connection->quoteInto('label_store.option_id = cpei.value AND label_store.store_id = ?', [$storeId]),
                []
            );
        $select->where('cpsl.parent_id IN (?)', $productIds)->columns([
            'cpsl.parent_id',
            'cpsl.product_id',
            'cpsa.product_super_attribute_id AS id',
            'cpsa.position',
            'cpsa.attribute_id',
            'ea.attribute_code',
            'cpei.value AS value_index',
            'IF(label_store.value IS NULL, label_default.value, label_store.value) AS option_label',
            'IF(attr_store.value IS NULL, attr_default.value, attr_store.value) AS attribute_label '
        ])->order('option.sort_order');

        $rows = $this->connection->fetchAll($select);

        $result     = [];
        $realResult = [];
        foreach ($rows as $row) {
            $result[$row['parent_id']][$row['id']]['attribute_id']                = $row['attribute_id'];
            $result[$row['parent_id']][$row['id']]['attribute_code']              = $row['attribute_code'];
            $result[$row['parent_id']][$row['id']]['product_id']                  = $row['parent_id'];
            $result[$row['parent_id']][$row['id']]['id']                          = $row['id'];
            $result[$row['parent_id']][$row['id']]['position']                    = $row['position'];
            $result[$row['parent_id']][$row['id']]['label']                       = $row['attribute_label'];
            $result[$row['parent_id']][$row['id']]['values'][$row['value_index']] = [
                'value_index' => (int)$row['value_index'],
                'label'       => $row['option_label']
            ];

            if (!isset($realResult[$row['parent_id']][$row['attribute_code']])
                || !in_array($row['value_index'], $realResult[$row['parent_id']][$row['attribute_code']])) {
                $realResult[$row['parent_id']][$row['attribute_code']][]              = $row['value_index'];
                $realResult[$row['parent_id']][$row['attribute_code'] . '_options'][] = $row['value_index'];
            }
        }

        foreach ($result as $entityId => $value) {
            $realResult[$entityId]['configurable_options'] = array_values($value);
            foreach ($realResult[$entityId]['configurable_options'] as $id => &$data) {
                $data['values'] = array_values($data['values']);
            }
        }

        return $realResult;
    }

    /**
     * @param array $productIds
     * @return array
     */
    public function getMediaGallery(array $productIds): array
    {
        $select = $this->connection->select()
            ->useStraightJoin(true)
            ->from(['e' => $this->getTable('catalog_product_entity_media_gallery_value_to_entity')])
            ->join(
                ['cpemg' => $this->getTable('catalog_product_entity_media_gallery')],
                'cpemg.value_id = e.value_id'
            )
            ->joinLeft(
                ['cpemgv' => $this->getTable('catalog_product_entity_media_gallery_value')],
                'e.value_id = cpemgv.value_id',
                []
            );
        $select->where('e.entity_id IN (?)', $productIds)->columns([
            'e.entity_id',
            'cpemg.value as image',
            'cpemg.media_type',
            'cpemgv.label',
            'cpemgv.disabled',
            'cpemgv.position',
        ]);
        $select->group('cpemg.value_id');
        $rows = $this->connection->fetchAll($select);

        $result = [];
        foreach ($rows as $row) {
            if (!$row['disabled']) {
                $result[$row['entity_id']][] = [
                    'image' => $row['image'],
                    'pos'   => $row['position'],
                    'typ'   => $row['media_type'],
                    'label' => $row['label'],
                ];
            }
        }
        return $result;
    }

    public function getBrandData(array $productIds, $storeId): array
    {
        $select = $this->connection->select()
            ->from(['e' => $this->getTable('catalog_product_entity')], ['entity_id', 'sku']);

        // Join brand attribute
        $this->joinAttribute($select, 'brand', $storeId, [], 'catalog_product');
        // Join brand option label
        $select->join(
            ['label_default' => $this->getTable('eav_attribute_option_value')],
            $this->connection->quoteInto('label_default.option_id = IF(brand_store.value IS NULL, brand_default.value, brand_store.value) AND label_default.store_id = ?', [0]),
            []
        )->joinLeft(
            ['label_store' => $this->getTable('eav_attribute_option_value')],
            $this->connection->quoteInto('label_store.option_id = IF(brand_store.value IS NULL, brand_default.value, brand_store.value) AND label_store.store_id = ?', [$storeId]),
            []
        );

        $select->columns('IF(label_store.value IS NULL, label_default.value, label_store.value) as brand');
        $select->where('e.entity_id IN (?)', $productIds);

        $rows = $this->connection->fetchAll($select);

        $result = [];
        foreach ($rows as $row) {
            $result[$row['entity_id']] = ['brand_data' => ['name' => $row['brand']]];
        }

        return $result;
    }

    public function getStock(array $productIds)
    {
        $select = $this->connection->select()
            ->from(['e' => $this->getTable('cataloginventory_stock_item')]);
        $select->where('e.product_id IN (?)', $productIds);
        $rows = $this->connection->fetchAll($select);

        $booleans = [
            "enable_qty_increments",
            "is_decimal_divided",
            "is_in_stock",
            "is_qty_decimal",
            "manage_stock",
            "show_default_notification_message",
            "use_config_backorders",
            "use_config_enable_qty_inc",
            "use_config_manage_stock",
            "use_config_max_sale_qty",
            "use_config_min_qty",
            "use_config_notify_stock_qty",
            "use_config_qty_increments",
        ];

        $result = [];

        foreach ($rows as $row) {
            foreach ($row as $key => &$value) {
                if (in_array($key, $booleans)) {
                    $value = (bool)$value;
                }
            }

            $result[$row['product_id']] = $row;
        }
        return $result;
    }

    /**
     * @param array $productIds
     * @param $websiteId
     * @param $storeId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPrices(array $productIds, $websiteId, $storeId): array
    {
        // Fetch "regular" prices and special prices, combined with any catalogrule prices.
        $select = $this->connection->select()
            ->from(['e' => $this->getTable('catalog_product_entity')], ['entity_id', 'sku']);

        $select->joinLeft(['cpp' => $this->getTable('catalogrule_product_price')],
            $this->connection->quoteInto('cpp.product_id = e.entity_id AND cpp.website_id = ? AND cpp.customer_group_id = 0 AND cpp.rule_date = CURDATE()', [$websiteId]),
            []);

        $this->joinAttribute($select, 'price', $storeId, [], 'catalog_product');
        $this->joinAttribute($select, 'special_price', $storeId, [], 'catalog_product', 'joinLeft');

        $select->columns([
            'IF (price_store.value IS NULL, price_default.value, price_store.value ) AS price',
            'IF (special_price_store.value IS NULL, special_price_default.value, special_price_store.value ) AS special_price',
            'cpp.rule_price'
        ]);

        $select->where('e.entity_id IN (?)', $productIds);

        $results = [];
        $rows    = $this->connection->fetchAll($select);
        foreach ($rows as $row) {
            // compare price, special_price and rule_price, lowest wins and becomes final_price
            $compare = [$row['price']];
            if ($row['special_price']) {
                $compare[] = $row['special_price'];
            }
            if ($row['rule_price']) {
                $compare[] = $row['rule_price'];
            }

            $results[$row['entity_id']] = [
                    'final_price'   => (float)min($compare),
                    'special_price' => (float)min($compare)
                ] + $row;
        }

        // Find tier prices
        $select = $this->connection->select()
            ->from(['e' => $this->getTable('catalog_product_entity')], ['entity_id']);

        $select->join(['cpetp' => $this->getTable('catalog_product_entity_tier_price')],
            $this->connection->quoteInto('cpetp.entity_id = e.entity_id AND cpetp.website_id IN(?) AND cpetp.customer_group_id = 0',
                [
                    [
                        0,
                        $websiteId
                    ]
                ]),
            ['*']);

        $select->where('e.entity_id IN (?)', $productIds);

        $rows = $this->connection->fetchAll($select);

        foreach ($rows as $row) {
            if ($row['percentage_value']) {
                $row['value'] = ($results[$row['entity_id']]['price'] * (100 - $row['percentage_value']) / 100);
            }
            $results[$row['entity_id']]['tier_prices'][] = [
                "customer_group_id" => $row['all_groups'] ? 32000 : $row['customer_group_id'],
                "value"             => (float)$row['value'],
                "qty"               => $row['qty']
            ];

            // If tierprice is for all products and from qty 1, use as final and special price aswell
            if ($row['all_groups'] && $row['qty'] == 1) {
                $results[$row['entity_id']]['special_price'] = (float)min($results[$row['entity_id']]['special_price'], $row['value']);
                $results[$row['entity_id']]['final_price']   = (float)min($results[$row['entity_id']]['final_price'], $row['value']);
            }
        }

        return $results;
    }

    public function translateAttributeIdsToCode($data, $unsetEmpty = false): array
    {
        $newData = [];
        foreach ($data as $key => $value) {
            if ($unsetEmpty && is_null($value)) {
                continue;
            }

            if (isset($this->attributes[$key])) {
                $newData[$this->attributes[$key]->getAttributeCode()] = $value;
            } else {
                $newData[$key] = $value;
            }
        }
        return $newData;
    }

    /**
     * @param $productData
     * @param $newArray
     * @return array
     */
    public function arrayAdd($productData, $newArray)
    {
        $productData = array_diff_key($productData, array_flip(array_keys($newArray)));
        $productData = $productData + $newArray;
        return $productData;
    }

    /**
     * @return mixed
     */
    public function getScopeId()
    {
        return $this->scopeId;
    }

    /**
     * @param mixed $scopeId
     */
    public function setScopeId($scopeId)
    {
        $this->scopeId = $scopeId;
    }

    protected function getSkus(array $productIds)
    {
        // Fetch "regular" prices and special prices, combined with any catalogrule prices.
        $select = $this->connection->select()
            ->from(['e' => $this->getTable('catalog_product_entity')], ['entity_id', 'sku']);
        $select->where('e.entity_id IN (?)', $productIds);
        return $this->connection->fetchPairs($select);
    }

    protected function getRelatedProducts($products)
    {
        $relatedProducts = [];
        foreach ($products as $productData) {
            $relatedProducts[$productData['entity_id']] = $this->dataProvider->getProductChildIds(
                $productData['entity_id'],
                $productData['type_id']
            );
        }
        return array_filter($relatedProducts);
    }

    protected function getSplitData(array $productIds, $websiteId, $storeId): array
    {
        $select = $this->connection->select()
            ->from(['e' => $this->getTable('catalog_product_entity')], ['entity_id', 'sku']);
        $this->joinAttribute($select, 'split_sku', $storeId, [], 'catalog_product');
        $select->columns('IF(split_sku_store.value IS NULL, split_sku_default.value, split_sku_store.value) as split_sku');
        $select->where('e.entity_id IN (?)', $productIds);

        $rows = $this->connection->fetchAll($select);

        $splitSkus = [];

        foreach ($rows as $row) {
            $splitSkus[$row['split_sku']][] = $row['entity_id'];
        }

        $select = $this->connection->select()
            ->from(['e' => $this->getTable('catalog_product_entity')], ['entity_id', 'sku']);
        $this->joinAttribute($select, 'thumbnail', $storeId, [], 'catalog_product', 'joinLeft');
        $this->joinAttribute($select, 'split_sku', $storeId, [array_keys($splitSkus)], 'catalog_product');
        $this->joinAttribute($select, 'filter_color', $storeId, [], 'catalog_product');
        $select->join(
            ['label_default' => $this->getTable('eav_attribute_option_value')],
            $this->connection->quoteInto('label_default.option_id = IF(filter_color_store.value IS NULL, filter_color_default.value, filter_color_store.value) AND label_default.store_id = ?', [0]),
            []
        )->join(
            ['label_store' => $this->getTable('eav_attribute_option_value')],
            $this->connection->quoteInto('label_store.option_id = IF(filter_color_store.value IS NULL, filter_color_default.value, filter_color_store.value) AND label_store.store_id = ?', [$storeId]),
            []
        );

        $select->columns([
            'IF(split_sku_store.value IS NULL, split_sku_default.value, split_sku_store.value) as split_sku',
            'IF(thumbnail_store.value IS NULL, thumbnail_default.value, thumbnail_store.value) as thumbnail',
            'IF(label_store.value IS NULL, label_default.value, label_store.value) as label',
        ]);
        $splitRows = $this->connection->fetchAll($select);

        $skus = array_column($splitRows, 'sku');
        $skus = $this->syncProduct->filterConfigurableWithoutChildren($skus, $websiteId);

        $splitData = [];
        foreach ($splitRows as $row) {
            if (in_array($row['sku'], $skus)) {
                foreach ($splitSkus[$row['split_sku']] as $entityId) {
                    $splitData[$entityId]['split_data']['linked_products'][] = [
                        'id'        => $row['entity_id'],
                        'thumbnail' => $row['thumbnail'],
                        'color'     => $row['label'],
                    ];

                    $splitData[$entityId]['split_data']['count'] = count($splitData[$entityId]['split_data']['linked_products']);
                }
            }
        }
        return $splitData;
    }
}
