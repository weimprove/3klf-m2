<?php

namespace Improving\VSFDirectSync\Model\Indexer\Product\Type;

class Configurable extends Base
{
    public function buildProductData($productsIds, $websiteId, $storeId, $staticFields, $pageSize)
    {
        $dynamicFields = [
            'int'      => array_keys($this->getAttributes('int')),
            'varchar'  => array_keys($this->getAttributes('varchar')),
            'text'     => array_keys($this->getAttributes('text')),
            'decimal'  => array_keys($this->getAttributes('decimal')),
            'datetime' => array_keys($this->getAttributes('datetime')),
        ];

        $chunk = [];

        $products = $this->getProducts($productsIds, $websiteId, $storeId, $staticFields, $pageSize);

        $relatedProducts = $this->getRelatedProducts($products);

        $categories = $this->getCategories($productsIds, $websiteId, $storeId);

        $configurableOptions = $this->getConfigurableOptions($productsIds, $storeId);

        $mediaGallery = $this->getMediaGallery($productsIds);

        // product links
        $splitData = $this->getSplitData($productsIds, $websiteId, $storeId);

        // brandData
        $brandData = $this->getBrandData($productsIds, $storeId);

        // Stock
        $stock = $this->getStock($productsIds);

        $productsIds = array_merge($productsIds, array_values($relatedProducts));

        // Price
        $prices = $this->getPrices($productsIds, $websiteId, $storeId);

        $productsAttributes = $this->dataProvider->getProductAttributes($storeId, $productsIds, $dynamicFields);

        foreach ($products as $productData) {
            $productData['id'] = $productData['entity_id'];
            $productData       = $productData + $this->translateAttributeIdsToCode($productsAttributes[$productData['entity_id']]);
            $productData       = $productData + ($categories[$productData['entity_id']] ?? []);
            $productData       = $productData + ($brandData[$productData['entity_id']] ?? []);
            $productData       = $productData + ($splitData[$productData['entity_id']] ?? []);

            $productData = $this->arrayAdd($productData, ($configurableOptions[$productData['entity_id']] ?? []));

            $productData = $productData + ['media_gallery' => $mediaGallery[$productData['entity_id']] ?? []];
            $productData = $productData + ['stock' => $stock[$productData['entity_id']] ?? []];

            if (isset($relatedProducts[$productData['entity_id']])) {
                foreach ($relatedProducts[$productData['entity_id']] as $childProductId) {
                    $childProduct = $this->translateAttributeIdsToCode($productsAttributes[$childProductId], true);
                    $childProduct = $this->arrayAdd($childProduct, ($prices[$childProductId] ?? []));

                    if (!isset($childProduct['entity_id'])) {
                        continue;
                    }

                    $childProduct['parent_id'] = $productData['entity_id'];
                    $childProduct['id']        = $childProduct['entity_id'];

                    $productData['configurable_children'][] = $childProduct;

                    if (!isset($productData['price'])) {
                        $productData['price'] = $childProduct['price'];
                    }
                    if (!isset($productData['final_price'])) {
                        $productData['final_price'] = $childProduct['final_price'];
                    }
                    $productData['final_price'] = min($productData['final_price'], $childProduct['final_price']);
                    $productData['price']       = max($productData['price'], $childProduct['price']);
                    if ($productData['final_price'] != $productData['price']) {
                        $productData['special_price'] = $childProduct['final_price'];
                    }
                }
            }

            $productData = $this->beforeAddToChunk($productData);
            $chunk[]     = $productData;
        }

        return $chunk;
    }
}
