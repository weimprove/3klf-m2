<?php

namespace Improving\VSFDirectSync\Model\Indexer\Product;

use Improving\VSFDirectSync\Model\DataProviderList;
use Improving\VSFDirectSync\Model\Sync\Product as SyncProduct;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use \Magento\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider as FullTextDataProvider;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Model\Config;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;
use Magento\Store\Model\Store;

class DataProvider
{
    /**
     * @var CollectionFactory
     */
    protected $prodAttributeCollectionFactory;
    /**
     * @var FullTextDataProvider
     */
    protected $dataProvider;
    /**
     * @var Config
     */
    protected $eavConfig;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;
    protected $scopeId;
    /**
     * @var SyncProduct
     */
    protected $syncProduct;
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;
    /**
     * @var DataProviderList
     */
    protected $dataProviderList;
    private $attributes;
    private $attributesByBackendType = [];
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * DataProvider constructor.
     * @param CollectionFactory $prodAttributeCollectionFactory
     * @param ResourceConnection $resourceConnection
     * @param Config $eavConfig
     * @param AttributeRepositoryInterface $attributeRepository
     * @param FullTextDataProvider $dataProvider
     */
    public function __construct(
        CollectionFactory $prodAttributeCollectionFactory,
        ResourceConnection $resourceConnection,
        Config $eavConfig,
        AttributeRepositoryInterface $attributeRepository,
        FullTextDataProvider $dataProvider,
        DataProviderList $dataProviderList,
        SyncProduct $syncProduct,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->prodAttributeCollectionFactory = $prodAttributeCollectionFactory;
        $this->dataProvider                   = $dataProvider;
        $this->eavConfig                      = $eavConfig;
        $this->resourceConnection             = $resourceConnection;
        $this->resource                       = $resourceConnection;
        $this->connection                     = $this->resourceConnection->getConnection();
        $this->attributeRepository            = $attributeRepository;
        $this->syncProduct                    = $syncProduct;
        $this->productCollectionFactory       = $productCollectionFactory;
        $this->dataProviderList               = $dataProviderList;
    }

    public function rebuildStoreIndex($websiteId, $storeId, $productIds, $dataSaver)
    {
        $staticFields = ['sku', 'attribute_set_id', 'has_options', 'created_at', 'updated_at'];

        $pageSize = 500;
        // loop here
        $products = $this->getProducts($productIds, $websiteId, $storeId, $staticFields, $pageSize);

        $providers = $this->dataProviderList->getProviders();

        while (count($products) > 0) {
            $time = time();
            $lastProductId = 0;

            $typeMap = [];
            foreach ($products as $p) {
                $typeMap[$p['type_id']][] = $p['entity_id'];

                $lastProductId = max($lastProductId, $p['entity_id']);
            }

            foreach ($typeMap as $type => $productsIds) {
                if (isset($providers[$type])) {
                    $providers[$type]->setScopeId($storeId);

                    $chunk = $providers[$type]->buildProductData($productsIds, $websiteId, $storeId, $staticFields, $pageSize);

                    $dataSaver->put($chunk, $storeId);
                }
            }

            $products = $this->getProducts($productIds, $websiteId, $storeId, $staticFields, $pageSize, $lastProductId);
        }
    }

    public function getProducts($productIds, $websiteId, $storeId, $staticFields, $pageSize, $lastProductId = 0)
    {
        $select = $this->connection->select()
            ->useStraightJoin(true)
            ->from(
                ['e' => $this->getTable('catalog_product_entity')],
                array_merge(['entity_id', 'type_id'], $staticFields)
            )
            ->join(
                ['website' => $this->getTable('catalog_product_website')],
                $this->connection->quoteInto('website.product_id = e.entity_id AND website.website_id = ?', $websiteId),
                []
            );

        if ($productIds !== null) {
            $select->where('e.entity_id IN (?)', $productIds);
        }
        $select->where('e.entity_id > ?', $lastProductId ? $lastProductId : 0);
        $select->order('e.entity_id');
        $select->limit($pageSize);

        return $this->connection->fetchAll($select);
    }

    /**
     * Return validated table name
     *
     * @param string|string[] $table
     * @return string
     */
    private function getTable($table)
    {
        return $this->resource->getTableName($table);
    }

}
