<?php

namespace Improving\VSFDirectSync\Model\Indexer;

class Noop implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface
{

    public function executeFull()
    {
        // No-op
        return true;
    }

    public function executeList(array $ids)
    {
        // No-op
        return true;
    }

    public function execute($ids)
    {
        // No-op
        return true;
    }

    public function executeRow($id)
    {
        // No-op
        return true;
    }
}
