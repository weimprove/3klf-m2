<?php

namespace Improving\VSFDirectSync\Model\Sync;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\CategoryFactory as CategoryFactory;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\InventorySalesApi\Api\StockResolverInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;

class Product
{
    protected $method = 'categoryCollection';
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    private $filterConfigurableWithoutChildrenCache = [];
    /**
     * @var Visibility
     */
    private $productVisibility;
    /**
     * @var StockResolverInterface
     */
    private $stockResolver;
    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * Product constructor.
     * @param CategoryFactory $categoryFactory
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param Visibility $productVisibility
     * @param StockResolverInterface $stockResolver
     * @param WebsiteRepositoryInterface $websiteRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        CategoryFactory $categoryFactory,
        CollectionFactory $collectionFactory,
        StockResolverInterface $stockResolver,
        Visibility $productVisibility,
        WebsiteRepositoryInterface $websiteRepository
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryFactory           = $categoryFactory;
        $this->collectionFactory         = $collectionFactory;
        $this->productVisibility         = $productVisibility;
        $this->stockResolver             = $stockResolver;
        $this->websiteRepository         = $websiteRepository;
    }

    public function getCategory($categoryId)
    {
        $category = $this->categoryFactory->create();
        $category->load($categoryId);
        return $category;
    }

    public function resolveProductsForSync($storeId, $websiteId, $searchSku = false, $stockCheck = true)
    {
        $collection = $this->collectionFactory->create();
        $collection->addWebsiteFilter($websiteId);
        $collection->addStoreFilter($storeId);
        $collection->addFieldToFilter('status', Status::STATUS_ENABLED);
        $collection->addAttributeToFilter('visibility', $this->productVisibility->getVisibleInSiteIds());

        if ($searchSku) {
            $collection->addFieldToFilter('sku', ['like' => '%' . $searchSku . '%']);
        }

        $collection->addFieldToSelect('id', 'sku');

        $skus = [];
        foreach ($collection as $product) {
            $activeIds[$product->getId()] = $product->getId();
            $skus[$product->getId()]      = $product->getSku();
        }

        $skus = $this->filterProductWithoutCategories($skus, $storeId);
        if ($stockCheck) {
            $skus = $this->filterConfigurableWithoutChildren($skus, $websiteId);
        }

        echo 'Total number or products: ' . count(array_unique($skus)) . PHP_EOL;
        return array_unique($skus);
    }

    public function filterProductWithoutCategories($skus, $storeId)
    {
        $sql = "
        SELECT
    product_id
FROM
    catalog_category_product
WHERE
    category_id IN (
    SELECT
        cce.entity_id
    FROM
        catalog_category_entity cce
    LEFT JOIN catalog_category_entity_int ccei ON
        ccei.entity_id = cce.entity_id
        AND ccei.attribute_id = 46
        AND ccei.store_id = 0
    WHERE
        ccei.value = 1 )
        ";

        $read   = $this->categoryCollectionFactory->create()->getResource()->getConnection('core_read');
        $result = $read->query($sql);

        $idsWithCategory = [];
        while ($row = $result->fetch()) {
            $idsWithCategory[] = $row['product_id'];
        }

        $okSkus = array_filter($skus, function ($value, $key) use ($idsWithCategory) {
                return (in_array($key, $idsWithCategory));
            }, ARRAY_FILTER_USE_BOTH) ?? [];

        return $okSkus;
    }

    public function filterConfigurableWithoutChildren($skus, $websiteId)
    {
        if (!isset($this->filterConfigurableWithoutChildrenCache[$websiteId])) {
            $websiteCode = $this->websiteRepository->getById($websiteId)->getCode();
            $stockId     = $this->stockResolver->execute('website', $websiteCode)->getStockId();

            // Get status of all children to configurable products.
            $sql = "
SELECT
    sku,
    status,stock
FROM
    (
    SELECT
        e.sku, avg(`at_status_l`.`value`) AS status, avg(i.is_salable) AS stock
    FROM
        catalog_product_entity e
    LEFT JOIN catalog_product_super_link l ON
        e.entity_id = l.parent_id
    LEFT JOIN `catalog_product_entity_int` AS `at_status_l` ON
        (`at_status_l`.`entity_id` = l.`product_id`)
    AND (`at_status_l`.`attribute_id` = '97')
        AND (`at_status_l`.`store_id` = 0)
    LEFT JOIN catalog_product_entity e2 ON l.product_id = e2.entity_id
    LEFT JOIN inventory_stock_${stockId} i ON i.sku = e2.sku
    WHERE
        e.type_id = 'configurable'
    GROUP BY
        e.entity_id ) AS a
WHERE
    (stock = 0
    OR status = 2
    OR stock IS NULL
OR status IS NULL)
";

            $read   = $this->categoryCollectionFactory->create()->getResource()->getConnection('core_read');
            $result = $read->query($sql);
            while ($row = $result->fetch()) {
                $this->filterConfigurableWithoutChildrenCache[$websiteId][] = $row;
            }
        }
        // With this info, check if products is in array of skus to index, if it is and all children are disabled,
        // remove the parent from list of products to index
        $skus = array_flip($skus);
        foreach ($this->filterConfigurableWithoutChildrenCache[$websiteId] as $row) {
            $sku = $row['sku'];
            unset($skus[$sku]);
        }

        $skus = array_flip($skus);
        return $skus;
    }
}
