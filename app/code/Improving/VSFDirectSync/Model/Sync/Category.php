<?php

namespace Improving\VSFDirectSync\Model\Sync;

use Elasticsearch\ClientBuilder;
use Improving\VSFDirectSync\Helper\Data;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Helper\Output;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Improving\VSFDirectSync\Helper\Cms\Widget;

class Category
{
    public $categoryCache;

    protected $skipAttributes = [
        "top_three_category",
        "top_category_anchor",
        "show_on_top_category",
        "is_anchor",
    ];
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;
    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var FilterProvider
     */
    private $filterProvider;
    /**
     * @var Output
     */
    private $catalogOutputHelper;
    /**
     * @var Widget
     */
    private $widgetHelper;
    /**
     * @var Category
     */
    private $category;
    /**
     * @var State
     */
    private $state;
    private $esClient;
    private $index;

    public function __construct(
        CollectionFactory $collectionFactory,
        CategoryRepositoryInterface $categoryRepository,
        BlockRepositoryInterface $blockRepository,
        Emulation $emulation,
        Data $helper,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        FilterProvider $filterProvider,
        Output $catalogOutputHelper,
        Widget $widgetHelper,
        State $state
    ) {
        $this->esClient            = ClientBuilder::create()->build();
        $this->collectionFactory   = $collectionFactory;
        $this->categoryRepository  = $categoryRepository;
        $this->blockRepository     = $blockRepository;
        $this->emulation           = $emulation;
        $this->helper              = $helper;
        $this->scopeConfig         = $scopeConfig;
        $this->storeManager        = $storeManager;
        $this->filterProvider      = $filterProvider;
        $this->catalogOutputHelper = $catalogOutputHelper;
        $this->widgetHelper        = $widgetHelper;
        $this->state               = $state;
    }

    public function indexForStore($storeId, $rootCategoryId, $index)
    {
        $this->emulation->startEnvironmentEmulation($storeId);
        $this->index         = $index;
        $this->categoryCache = [];

        $activeIds   = [];
        $inactiveIds = [];

        $this->getCategoryData($rootCategoryId, $storeId);
        $this->getCategoryUrlPath();

        $inactiveIds = $this->findInactives($this->categoryCache, $inactiveIds);

        foreach ($inactiveIds as $id) {
            unset($this->categoryCache[$id]);
        }

        foreach ($this->categoryCache as $category) {
            $params                     = [
                'index' => $this->index,
                'type'  => 'category',
                'id'    => $category['id'],
                'body'  => $category,
            ];
            $activeIds[$category['id']] = $category['id'];

            $response = $this->esClient->index($params);
        }

        $esIds = $this->helper->getEsIds($this->index, 'category');

        foreach ($esIds as $id) {
            if (!in_array($id, $activeIds, true)) {

                $this->esClient->delete([
                    'index' => $this->index,
                    'type'  => 'category',
                    'id'    => $id,
                ]);
                echo 'DELETE FROM index: ' . $id . PHP_EOL;
            }
        }
    }

    public function getCategoryData($categoryId, $storeId, $buildUrlPath = true)
    {
        $category = $this->categoryRepository->get($categoryId, $storeId);

        $categoryData = $category->getData();

        $this->getFilteredContent($category, $categoryData);
        $categoryData['children_data'] = [];
        $categoryData['id']            = (int)$category->getId();
        $categoryData['parent_id']     = (int)$category->getParentId();
        $categoryData['product_count'] = 100;
        if (isset($categoryData['url_key'])) {
            $categoryData['slug'] = $categoryData['url_key'] . '-' . $categoryData['id'];
        } else {
            $categoryData['slug'] = 'category-' . $categoryData['id'];
        }

        $categoryData['include_in_menu'] = (bool)$categoryData['include_in_menu'];
        $categoryData['is_active']       = (bool)$category->getIsActive();
        $categoryData['children']        = (string)$category->getChildren() . ' ';

        $categoryData['active_filters'] = $this->getActiveFilters($categoryId);

        if (isset($categoryData['landing_page'])) {
            $categoryData['landing_page_identifier'] = $this->getStaticBlockIdentifier($categoryData['landing_page']);
        }
        array_walk_recursive($categoryData, [$this->helper, 'convertNumber'], $this->skipAttributes);
        $this->widgetHelper->convertWidgetFieldsData($categoryData);

        $whitelist = $this->getChildrenDataAttributes();
        if ($category->getChildren()) {
            $children = explode(',', $category->getChildren());
            foreach ($children as $childId) {
                $this->getCategoryData($childId, $storeId);
                $filteredCategoryData            = array_intersect_key($this->categoryCache[$childId], array_flip($whitelist));
                $categoryData['children_data'][] = $filteredCategoryData;
            }
        }

        $this->categoryCache[$category->getId()] = $categoryData;

        return $categoryData;
    }

    protected function getFilteredContent($category, &$categoryData)
    {
        $widgetWysiwygFields = ['description', 'megamenu_widget_content'];

        foreach ($widgetWysiwygFields as $field) {
            $widget = $this->widgetHelper->parseCmsPageWidgetData($category->getData($field));

            if ($widget) {
                $categoryData[$field]               = '';
                $categoryData[$field . '_elements'] = $widget;
            } else {
                $categoryData[$field] = $this->filterProvider
                    ->getBlockFilter()
                    ->filter(
                        $this->catalogOutputHelper->categoryAttribute(
                            $category,
                            $category->getData($field),
                            $field
                        )
                    );
            }
        }
    }

    private function getActiveFilters(
        $categoryId
    ) {
        $read = $this->collectionFactory->create()->getConnection('read');

        $attributes = [];
        try {
            $rows = $read->fetchAll('SELECT * FROM amasty_amshopby_filter_setting WHERE FIND_IN_SET(' . $categoryId . ', categories_filter)');

            foreach ($rows as $row) {

                $attributeCode = str_replace('attr_', '', $row['filter_code']);
                $attributes[]  = $attributeCode;
            }
        } catch (\Exception $e) {

        }
        return $attributes;
    }

    protected function getStaticBlockIdentifier(
        $id
    ) {
        if (!isset($this->blockCache[$id])) {
            $block                 = $this->blockRepository->getById($id);
            $this->blockCache[$id] = $block->getIdentifier();
        }

        return $this->blockCache[$id];
    }

    public function getChildrenDataAttributes()
    {
        $whitelist = ['level', 'url_key', 'id', 'parent_id', 'is_active', 'children_data'];
        return $whitelist;

    }

    private function getCategoryUrlPath()
    {
        foreach ($this->categoryCache as $key => $category) {
            $level   = $category['level'];
            $urlPath = $category['url_key'] ?? '';
            while ($level > 2) {
                $category = $this->categoryCache[$category['parent_id']];
                $level    = $category['level'];
                $urlPath  = $category['url_key'] . '/' . $urlPath;
            }

            $this->categoryCache[$key]['url_path'] = $urlPath;
        }
    }

    protected function findInactives(
        $categories,
        $inactiveIds,
        $inactive = false
    ) {
        foreach ($categories as $category) {
            if (!$category['is_active'] || $inactive) {
                $inactiveIds   = $this->findInactives($category['children_data'], $inactiveIds, true);
                $inactiveIds[] = $category['id'];
            }
        }
        return $inactiveIds;
    }

    public function indexIdsForStore($ids, $storeId, $index = false)
    {
        $this->categoryCache = [];
        $inactiveIds         = [];

        foreach ($ids as $id) {
            $data = $this->getCategoryData($id, $storeId);

            if (!isset($this->categoryCache[$data['parent_id']]) && $data['level'] > 2) {
                $this->indexIdsForStore([$data['parent_id']], $storeId);
            }
        }

        if ($index) {
            $this->getCategoryUrlPath();
            $inactiveIds = $this->findInactives($this->categoryCache, $inactiveIds);

            foreach ($inactiveIds as $id) {
                unset($this->categoryCache[$id]);

                try {
                    $this->esClient->delete([
                        'index' => $index,
                        'type'  => 'category',
                        'id'    => $id,
                    ]);
                } catch (\Exception $e) {
                }
            }

            foreach ($this->categoryCache as $category) {
                $params                     = [
                    'index' => $index,
                    'type'  => 'category',
                    'id'    => $category['id'],
                    'body'  => $category
                ];
                $activeIds[$category['id']] = $category['id'];

                $response = $this->esClient->index($params);
            }
        }
    }
}
