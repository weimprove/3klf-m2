<?php

namespace Improving\VSFDirectSync\Observer;

use Elasticsearch\ClientBuilder;
use Improving\VSFDirectSync\Helper\Cms\Widget;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filter\FilterManager;

class WidgetInstanceSaveAfter implements ObserverInterface
{
    /**
     * @var FilterProvider
     */
    protected $filterProvider;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var FilterManager
     */
    private $filterManager;
    /**
     * @var Widget
     */
    private $widgetHelper;

    /**
     * CmsBlockSaveAfter constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param FilterManager $filterManager
     * @param Widget $widgetHelper
     * @param FilterProvider $filterProvider
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        FilterManager $filterManager,
        Widget $widgetHelper,
        FilterProvider $filterProvider
    ) {
        $this->filterProvider = $filterProvider;
        $this->scopeConfig    = $scopeConfig;
        $this->filterManager  = $filterManager;
        $this->widgetHelper   = $widgetHelper;
    }

    public function execute(Observer $observer)
    {
        $block = $observer->getEvent()->getObject();
        $data  = $block->getData();

        if (isset($data['widget_parameters'])) {
            $esClient = ClientBuilder::create()->build();

            $params = json_decode($data['widget_parameters'], 1);
            $result = $this->widgetHelper->parseWidgetParameters($params);

            $data['identifier'] = $this->filterManager->translitUrl($data['title']);
            $data['widget_parameters']   = $result;

            $index  = $this->scopeConfig->getValue('vsfdirectsync/cms/index', 'websites');
            $params = [
                'index' => $index,
                'type'  => 'widget',
                'id'    => $data['instance_id'],
                'body'  => $data,
            ];
            try {
                $esClient->index($params);
            } catch (\Exception $e) {
                die(var_dump($e->getMessage()));
                exit;
            }
        }
    }

}