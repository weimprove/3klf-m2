<?php

namespace Improving\VSFDirectSync\Observer;

use Elasticsearch\ClientBuilder;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CmsPageDeleteAfter implements ObserverInterface
{
    /**
     * @var FilterProvider
     */
    protected $filterProvider;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * CmsPageDeleteAfter constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param FilterProvider $filterProvider
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        FilterProvider $filterProvider
    ) {
        $this->filterProvider = $filterProvider;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(Observer $observer)
    {
        $page = $observer->getEvent()->getObject();
        $data = $page->getData();

        $esClient = ClientBuilder::create()->build();

        $index = $this->scopeConfig->getValue('vsfdirectsync/cms/index', 'websites');

        $params = [
            'index' => $index,
            'type'  => 'page',
            'id'    => $data['page_id'],
        ];

        $esClient->delete($params);
    }
}