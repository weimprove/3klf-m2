<?php

namespace Improving\VSFDirectSync\Observer;

use Elasticsearch\ClientBuilder;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CmsBlockSaveAfter implements ObserverInterface
{
    /**
     * @var FilterProvider
     */
    protected $filterProvider;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * CmsBlockSaveAfter constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param FilterProvider $filterProvider
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        FilterProvider $filterProvider
    ) {
        $this->filterProvider = $filterProvider;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(Observer $observer)
    {
        $block = $observer->getEvent()->getObject();
        $data = $block->getData();
        $data['content'] = $this->getBlockContentFiltered($block->getContent());

        $esClient = ClientBuilder::create()->build();

        $index = $this->scopeConfig->getValue('vsfdirectsync/cms/index', 'websites');

        $params = [
            'index' => $index,
            'type'  => 'block',
            'id'    => $data['block_id'],
            'body'  => $data,
        ];
        $esClient->index($params);
    }

    /**
     * @param $content
     * @return string
     */
    private function getBlockContentFiltered($content)
    {
        $html = $this->filterProvider->getBlockFilter()
            ->filter($content);

        // Parse html, attach image src as data property aswell, support for lazyloading images
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        foreach ($dom->childNodes as $item) {
            if ($item->nodeType == XML_PI_NODE) {
                $dom->removeChild($item);
            }
        } // remove utf-8 hack

        $images = $dom->getElementsByTagName('img');

        $emptySrcAttr = $this->scopeConfig->getValue('vsfdirectsync/cms/clear_src_attr', 'websites');

        foreach ($images as $k => $img) {
            $src = $img->getAttribute('src');
            $img->setAttribute('data-src', $src);
            if ($emptySrcAttr) {
                $img->setAttribute('src', 'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8Xw8AAoMBgDTD2qgAAAAASUVORK5CYII=');
            }
        }

        $html = $dom->saveHTML();

        return $html;
    }
}