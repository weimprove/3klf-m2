<?php

namespace Improving\VSFDirectSync\Observer;

use Elasticsearch\ClientBuilder;
use Improving\VSFDirectSync\Helper\Cms\Widget;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CmsPageSaveAfter implements ObserverInterface
{
    /**
     * @var FilterProvider
     */
    protected $filterProvider;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var Widget
     */
    private $widgetHelper;

    /**
     * CmsPageSaveAfter constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Widget $widgetHelper
     * @param FilterProvider $filterProvider
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Widget $widgetHelper,
        FilterProvider $filterProvider
    ) {
        $this->filterProvider = $filterProvider;
        $this->scopeConfig    = $scopeConfig;
        $this->widgetHelper   = $widgetHelper;
    }

    public function execute(Observer $observer)
    {
        $page   = $observer->getEvent()->getObject();
        $data   = $page->getData();
        $widget = $this->widgetHelper->parseCmsPageWidgetData($page->getContent());
        if ($widget) {
            $data['content']  = '';
            $data['elements'] = $widget;
        } else {
            $data['content'] = $this->getBlockContentFiltered($data['content']);
        }

        $data['url_path']   = $data['identifier'];
        $data['identifier'] = str_replace(['-', '/'], '_', $data['identifier']);

        $esClient = ClientBuilder::create()->build();

        $index = $this->scopeConfig->getValue('vsfdirectsync/cms/index', 'websites');

        $params = [
            'index' => $index,
            'type'  => 'page',
            'id'    => $data['page_id'],
            'body'  => $data,
        ];
        $esClient->index($params);
    }

    private function getBlockContentFiltered(
        $content
    ) {
        return $this->filterProvider->getPageFilter()
            ->filter($content);
    }


}
