<?php
namespace Improving\Customer\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    private $eavConfig;
    private $attributeResource;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Customer\Model\ResourceModel\Attribute $attributeResource
    ){
        $this->eavSetupFactory = $eavSetupFactory;     $this->eavConfig = $eavConfig;
        $this->attributeResource = $attributeResource;
    }

    public function install(
        ModuleDataSetupInterface $setup, ModuleContextInterface $context
    ){
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(Customer::ENTITY, "customer_company");
        $eavSetup->removeAttribute(Customer::ENTITY, "customer_company_position");
        $eavSetup->removeAttribute(Customer::ENTITY, "customer_phone");
        $eavSetup->removeAttribute(Customer::ENTITY, "customer_linkedin");
        $eavSetup->removeAttribute(Customer::ENTITY, "customer_website");
        $eavSetup->removeAttribute(Customer::ENTITY, "customer_image");

        $attributeSetId = $eavSetup->getDefaultAttributeSetId(Customer::ENTITY);
        $attributeGroupId = $eavSetup->getDefaultAttributeGroupId(Customer::ENTITY);

        $eavSetup->addAttribute(Customer::ENTITY, 'customer_company', [
            // Attribute parameters
            'type' => 'varchar',
            'label' => 'Virksomhed',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 980,
            'position' => 980,
            'system' => 0,
        ]);
        $eavSetup->addAttribute(Customer::ENTITY, 'customer_company_position', [
            // Attribute parameters
            'type' => 'varchar',
            'label' => 'Stilling',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 981,
            'position' => 981,
            'system' => 0,
        ]);
        $eavSetup->addAttribute(Customer::ENTITY, 'customer_phone', [
            // Attribute parameters
            'type' => 'varchar',
            'label' => 'Mobil telefon',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 982,
            'position' => 982,
            'system' => 0,
        ]);
        $eavSetup->addAttribute(Customer::ENTITY, 'customer_linkedin', [
            // Attribute parameters
            'type' => 'varchar',
            'label' => 'Linkedin',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 983,
            'position' => 983,
            'system' => 0,
        ]);
        $eavSetup->addAttribute(Customer::ENTITY, 'customer_website', [
            // Attribute parameters
            'type' => 'varchar',
            'label' => 'Website',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 984,
            'position' => 984,
            'system' => 0,
        ]);
        $eavSetup->addAttribute(Customer::ENTITY, 'customer_image', [
            // Attribute parameters
            'type' => 'varchar',
            'label' => 'Billede',
            'input' => 'file',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 985,
            'position' => 985,
            'system' => 0,
        ]);

        $attribute_1 = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_company');
        $attribute_2 = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_company_position');
        $attribute_3 = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_phone');
        $attribute_4 = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_linkedin');
        $attribute_5 = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_website');
        $attribute_6 = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_image');

        $attribute_1->setData('attribute_set_id', $attributeSetId);
        $attribute_2->setData('attribute_set_id', $attributeSetId);
        $attribute_3->setData('attribute_set_id', $attributeSetId);
        $attribute_4->setData('attribute_set_id', $attributeSetId);
        $attribute_5->setData('attribute_set_id', $attributeSetId);
        $attribute_6->setData('attribute_set_id', $attributeSetId);

        $attribute_1->setData('attribute_group_id', $attributeGroupId);
        $attribute_2->setData('attribute_group_id', $attributeGroupId);
        $attribute_3->setData('attribute_group_id', $attributeGroupId);
        $attribute_4->setData('attribute_group_id', $attributeGroupId);
        $attribute_5->setData('attribute_group_id', $attributeGroupId);
        $attribute_6->setData('attribute_group_id', $attributeGroupId);


        $attribute_1->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->attributeResource->save($attribute_1);

        $attribute_2->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->attributeResource->save($attribute_2);

        $attribute_3->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->attributeResource->save($attribute_3);

        $attribute_4->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->attributeResource->save($attribute_4);

        $attribute_5->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->attributeResource->save($attribute_5);

        $attribute_6->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_create',
            'customer_account_edit'
        ]);
        $this->attributeResource->save($attribute_6);

    }
}

