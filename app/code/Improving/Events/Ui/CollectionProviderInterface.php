<?php

declare(strict_types=1);

namespace Improving\Events\Ui;

interface CollectionProviderInterface
{
    /**
     * @return \Improving\Events\Model\ResourceModel\AbstractCollection
     */
    public function getCollection();
}
