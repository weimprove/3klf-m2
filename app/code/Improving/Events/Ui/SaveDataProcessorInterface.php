<?php

declare(strict_types=1);

namespace Improving\Events\Ui;

interface SaveDataProcessorInterface
{
    /**
     * @param array $aata
     * @return array
     */
    public function modifyData(array $data): array;
}
