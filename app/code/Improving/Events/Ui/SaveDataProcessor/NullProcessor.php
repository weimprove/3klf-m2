<?php

declare(strict_types=1);

namespace Improving\Events\Ui\SaveDataProcessor;

use Improving\Events\Ui\SaveDataProcessorInterface;

class NullProcessor implements SaveDataProcessorInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data): array
    {
        return $data;
    }
}
