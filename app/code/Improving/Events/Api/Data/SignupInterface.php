<?php

declare(strict_types=1);

namespace Improving\Events\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface SignupInterface extends ExtensibleDataInterface
{
    public const SIGNUP_ID = 'signup_id';
    public const CUSTOMER_ID = 'customer_id';
    public const SIGNUP = 'signup';
    public const EVENT_ID = 'event_id';

    /**
     * @param int $id
     * @return SignupInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return SignupInterface
     */
    public function setSignupId($id);

    /**
     * @return int
     */
    public function getSignupId();

    /**
     * @param int $eventId
     * @return SignupInterface
     */
    public function setEventId($eventId);

    /**
     * @return int
     */
    public function getEventId();
    /**
     * Set Customer Id
     *
     * @param int $customerId
     * @return SignupInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get Customer Id
     *
     * @return int
     */
    public function getCustomerId();

    /**
     * Set Signup
     *
     * @param string $signup
     * @return SignupInterface
     */
    public function setSignup($signup);

    /**
     * Get Signup
     *
     * @return string
     */
    public function getSignup();

}
