<?php

declare(strict_types=1);

namespace Improving\Events\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface EventSearchResultsInterface
{
    /**
     * get items
     *
     * @return \Improving\Events\Api\Data\EventInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \Improving\Events\Api\Data\EventInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);
}
