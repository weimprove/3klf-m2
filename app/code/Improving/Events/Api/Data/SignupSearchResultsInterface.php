<?php

declare(strict_types=1);

namespace Improving\Events\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @api
 */
interface SignupSearchResultsInterface
{
    /**
     * get items
     *
     * @return \Improving\Events\Api\Data\SignupInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param \Improving\Events\Api\Data\SignupInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count);
}
