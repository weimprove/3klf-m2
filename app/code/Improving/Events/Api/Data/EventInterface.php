<?php

declare(strict_types=1);

namespace Improving\Events\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */
interface EventInterface extends ExtensibleDataInterface
{
    public const EVENT_ID = 'event_id';
    public const TITLE = 'title';
    public const IMAGE = 'image';
    public const DATE = 'date';
    public const SIGNUP_DATE = 'signup_date';
    public const LOCATION = 'location';
    public const ADDRESS = 'address';
    public const DESCRIPTION = 'description';

    /**
     * @param int $id
     * @return EventInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return EventInterface
     */
    public function setEventId($id);

    /**
     * @return int
     */
    public function getEventId();
    /**
     * Set Title
     *
     * @param string $title
     * @return EventInterface
     */
    public function setTitle($title);

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set Image
     *
     * @param string $image
     * @return EventInterface
     */
    public function setImage($image);

    /**
     * Get Image
     *
     * @return string
     */
    public function getImage();

    /**
     * Set Date
     *
     * @param string $date
     * @return EventInterface
     */
    public function setDate($date);

    /**
     * Get Date
     *
     * @return string
     */
    public function getDate();

    /**
     * Set Signup Date
     *
     * @param string $signupDate
     * @return EventInterface
     */
    public function setSignupDate($signupDate);

    /**
     * Get Signup Date
     *
     * @return string
     */
    public function getSignupDate();

    /**
     * Set Location
     *
     * @param string $location
     * @return EventInterface
     */
    public function setLocation($location);

    /**
     * Get Location
     *
     * @return string
     */
    public function getLocation();

    /**
     * Set Address
     *
     * @param string $address
     * @return EventInterface
     */
    public function setAddress($address);

    /**
     * Get Address
     *
     * @return string
     */
    public function getAddress();

    /**
     * Set Description
     *
     * @param string $description
     * @return EventInterface
     */
    public function setDescription($description);

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription();

}
