<?php

declare(strict_types=1);

namespace Improving\Events\Api;

/**
 * @api
 */
interface EventRepositoryInterface
{
    /**
     * @param \Improving\Events\Api\Data\EventInterface $event
     * @return \Improving\Events\Api\Data\EventInterface
     */
    public function save(\Improving\Events\Api\Data\EventInterface $event);

    /**
     * @param int $eventId
     * @return \Improving\Events\Api\Data\EventInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(int $eventId);

    /**
     * @param \Improving\Events\Api\Data\EventInterface $event
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Improving\Events\Api\Data\EventInterface $event);

    /**
     * @param int $eventId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById(int $eventId);

    /**
     * clear caches instances
     * @return void
     */
    public function clear();
}
