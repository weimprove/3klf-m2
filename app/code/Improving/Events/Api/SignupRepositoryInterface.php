<?php

declare(strict_types=1);

namespace Improving\Events\Api;

/**
 * @api
 */
interface SignupRepositoryInterface
{
    /**
     * @param \Improving\Events\Api\Data\SignupInterface $signup
     * @return \Improving\Events\Api\Data\SignupInterface
     */
    public function save(\Improving\Events\Api\Data\SignupInterface $signup);

    /**
     * @param int $signupId
     * @return \Improving\Events\Api\Data\SignupInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(int $signupId);

    /**
     * @param \Improving\Events\Api\Data\SignupInterface $signup
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Improving\Events\Api\Data\SignupInterface $signup);

    /**
     * @param int $signupId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById(int $signupId);

    /**
     * clear caches instances
     * @return void
     */
    public function clear();
}
