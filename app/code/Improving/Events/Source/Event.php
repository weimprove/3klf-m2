<?php

declare(strict_types=1);

namespace Improving\Events\Source;

use Improving\Events\Api\Data\EventInterface;
use Improving\Events\Api\EventListRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Data\OptionSourceInterface;

class Event implements OptionSourceInterface
{
    /**
     * @var EventListRepositoryInterface
     */
    private $repository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var array
     */
    private $options;

    /**
     * Event constructor.
     * @param EventListRepositoryInterface $repository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        EventListRepositoryInterface $repository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->repository = $repository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = array_map(
                function (EventInterface $event) {
                    return [
                        'label' => $event->getTitle(),
                        'value' => $event->getEventId()
                    ];
                },
                $this->repository->getList($this->searchCriteriaBuilder->create())->getItems()
            );
            uasort(
                $this->options,
                function (array $optionA, array $optionB) {
                    return strcmp($optionA['label'], $optionB['label']);
                }
            );
            $this->options = array_values($this->options);
        }
        return $this->options;
    }
}
