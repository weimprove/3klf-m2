<?php

declare(strict_types=1);

namespace Improving\Events\Source;

use Improving\Events\Api\Data\SignupInterface;
use Improving\Events\Api\SignupListRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Data\OptionSourceInterface;

class Signup implements OptionSourceInterface
{
    /**
     * @var SignupListRepositoryInterface
     */
    private $repository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var array
     */
    private $options;

    /**
     * Signup constructor.
     * @param SignupListRepositoryInterface $repository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        SignupListRepositoryInterface $repository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->repository = $repository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = array_map(
                function (SignupInterface $signup) {
                    return [
                        'label' => $signup->getSignup(),
                        'value' => $signup->getSignupId()
                    ];
                },
                $this->repository->getList($this->searchCriteriaBuilder->create())->getItems()
            );
            uasort(
                $this->options,
                function (array $optionA, array $optionB) {
                    return strcmp($optionA['label'], $optionB['label']);
                }
            );
            $this->options = array_values($this->options);
        }
        return $this->options;
    }
}
