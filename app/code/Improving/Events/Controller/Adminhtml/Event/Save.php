<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\HttpPostActionInterface;

class Save extends \Improving\Events\Controller\Adminhtml\Save implements HttpPostActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_event';
}
