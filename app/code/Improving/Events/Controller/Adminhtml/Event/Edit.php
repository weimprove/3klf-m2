<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\HttpGetActionInterface;

class Edit extends \Improving\Events\Controller\Adminhtml\Edit implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_event';
}
