<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\HttpPostActionInterface;

class InlineEdit extends \Improving\Events\Controller\Adminhtml\InlineEdit implements HttpPostActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_event';
}
