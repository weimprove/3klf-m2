<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Event;

use Magento\Framework\App\Action\HttpGetActionInterface;

class Index extends \Improving\Events\Controller\Adminhtml\Index implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_event';
}
