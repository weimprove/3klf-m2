<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Event\Image;

use Magento\Framework\App\Action\HttpPostActionInterface;

class Upload extends \Improving\Events\Controller\Adminhtml\Upload implements HttpPostActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_event';
}
