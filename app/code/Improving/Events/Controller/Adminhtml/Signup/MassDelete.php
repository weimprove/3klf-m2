<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Signup;

use Magento\Framework\App\Action\HttpPostActionInterface;

class MassDelete extends \Improving\Events\Controller\Adminhtml\MassDelete implements HttpPostActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_signup';
}
