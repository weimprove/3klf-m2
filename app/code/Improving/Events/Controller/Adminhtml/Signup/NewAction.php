<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Signup;

use Magento\Framework\App\Action\HttpGetActionInterface;

class NewAction extends \Improving\Events\Controller\Adminhtml\NewAction implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_signup';
}
