<?php

declare(strict_types=1);

namespace Improving\Events\Controller\Adminhtml\Signup;

use Magento\Framework\App\Action\HttpPostActionInterface;

class Delete extends \Improving\Events\Controller\Adminhtml\Delete implements HttpPostActionInterface
{
    public const ADMIN_RESOURCE = 'Improving_Events::events_signup';
}
