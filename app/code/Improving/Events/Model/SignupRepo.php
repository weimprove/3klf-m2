<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\SignupInterface;
use Improving\Events\Api\Data\SignupInterfaceFactory;
use Improving\Events\Api\SignupRepositoryInterface;
use Improving\Events\Model\ResourceModel\Signup as SignupResourceModel;

class SignupRepo implements SignupRepositoryInterface
{
    /**
     * @var SignupInterfaceFactory
     */
    private $factory;
    /**
     * @var SignupResourceModel
     */
    private $resource;
    /**
     * @var SignupInterface[]
     */
    private $cache = [];

    /**
     *
     */
    public function __construct(
        SignupInterfaceFactory $factory,
        SignupResourceModel $resource
    ) {
        $this->factory = $factory;
        $this->resource = $resource;
    }

    /**
     * @inheritdoc
     */
    public function save(SignupInterface $signup)
    {
        try {
            $this->resource->save($signup);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __($exception->getMessage())
            );
        }
        return $signup;
    }

    /**
     * @inheritdoc
     */
    public function get(int $signupId)
    {
        if (!isset($this->cache[$signupId])) {
            $signup = $this->factory->create();
            $this->resource->load($signup, $signupId);
            if (!$signup->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(
                    __('The Signup with the ID "%1" does not exist . ', $signupId)
                );
            }
            $this->cache[$signupId] = $signup;
        }
        return $this->cache[$signupId];
    }

    /**
     * @inheritdoc
     */
    public function delete(SignupInterface $signup)
    {
        try {
            $id = $signup->getId();
            $this->resource->delete($signup);
            unset($this->cache[$id]);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotDeleteException(
                __($exception->getMessage())
            );
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById(int $signupId)
    {
        return $this->delete($this->get($signupId));
    }

    /**
     * @inheritdoc
     */
    public function clear()
    {
        return $this->cache = [];
    }
}
