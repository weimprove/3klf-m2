<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\EventInterface;
use Improving\Events\Model\ResourceModel\Event as EventResourceModel;
use Magento\Framework\Model\AbstractExtensibleModel;

class Event extends AbstractExtensibleModel implements EventInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    public const CACHE_TAG = 'improving_events_event';
    /**
     * Cache tag
     *
     * @var string
     * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore,PSR12.Classes.PropertyDeclaration.Underscore
     */
    protected $_cacheTag = self::CACHE_TAG;
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'improving_events_event';
    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'event';
    //phpcs:enable
    /**
     * Initialize resource model
     *
     * @return void
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore,PSR12.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init(EventResourceModel::class);
        //phpcs:enable
    }

    /**
     * Get Event id
     *
     * @return array
     */
    public function getEventId()
    {
        return $this->getData(EventInterface::EVENT_ID);
    }

    /**
     * Set Event id
     *
     * @param  int $eventId
     * @return EventInterface
     */
    public function setEventId($eventId)
    {
        return $this->setData(self::EVENT_ID, $eventId);
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return EventInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set Image
     *
     * @param string $image
     * @return EventInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Get Image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * Set Date
     *
     * @param string $date
     * @return EventInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }

    /**
     * Get Date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->getData(self::DATE);
    }

    /**
     * Set Signup Date
     *
     * @param string $signupDate
     * @return EventInterface
     */
    public function setSignupDate($signupDate)
    {
        return $this->setData(self::SIGNUP_DATE, $signupDate);
    }

    /**
     * Get Signup Date
     *
     * @return string
     */
    public function getSignupDate()
    {
        return $this->getData(self::SIGNUP_DATE);
    }

    /**
     * Set Location
     *
     * @param string $location
     * @return EventInterface
     */
    public function setLocation($location)
    {
        return $this->setData(self::LOCATION, $location);
    }

    /**
     * Get Location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->getData(self::LOCATION);
    }

    /**
     * Set Address
     *
     * @param string $address
     * @return EventInterface
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Get Address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return EventInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritdoc
     *
     * @return \Improving\Events\Api\Data\EventExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritdoc
     *
     * @param \Improving\Events\Api\Data\EventExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Improving\Events\Api\Data\EventExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
