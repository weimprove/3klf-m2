<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Model\ResourceModel\Signup\CollectionFactory;
use Improving\Events\Ui\CollectionProviderInterface;

class SignupUiCollectionProvider implements CollectionProviderInterface
{
    /**
     * @var CollectionFactory
     */
    private $factory;

    /**
     * @param CollectionFactory $factory
     */
    public function __construct(CollectionFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getCollection()
    {
        return $this->factory->create();
    }
}
