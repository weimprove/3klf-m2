<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\EventInterface;
use Improving\Events\Api\Data\EventInterfaceFactory;
use Improving\Events\Api\EventRepositoryInterface;
use Improving\Events\Model\ResourceModel\Event as EventResourceModel;

class EventRepo implements EventRepositoryInterface
{
    /**
     * @var EventInterfaceFactory
     */
    private $factory;
    /**
     * @var EventResourceModel
     */
    private $resource;
    /**
     * @var EventInterface[]
     */
    private $cache = [];

    /**
     *
     */
    public function __construct(
        EventInterfaceFactory $factory,
        EventResourceModel $resource
    ) {
        $this->factory = $factory;
        $this->resource = $resource;
    }

    /**
     * @inheritdoc
     */
    public function save(EventInterface $event)
    {
        try {
            $this->resource->save($event);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __($exception->getMessage())
            );
        }
        return $event;
    }

    /**
     * @inheritdoc
     */
    public function get(int $eventId)
    {
        if (!isset($this->cache[$eventId])) {
            $event = $this->factory->create();
            $this->resource->load($event, $eventId);
            if (!$event->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(
                    __('The Event with the ID "%1" does not exist . ', $eventId)
                );
            }
            $this->cache[$eventId] = $event;
        }
        return $this->cache[$eventId];
    }

    /**
     * @inheritdoc
     */
    public function delete(EventInterface $event)
    {
        try {
            $id = $event->getId();
            $this->resource->delete($event);
            unset($this->cache[$id]);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotDeleteException(
                __($exception->getMessage())
            );
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById(int $eventId)
    {
        return $this->delete($this->get($eventId));
    }

    /**
     * @inheritdoc
     */
    public function clear()
    {
        return $this->cache = [];
    }
}
