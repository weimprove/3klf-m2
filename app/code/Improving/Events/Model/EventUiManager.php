<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\EventInterface;
use Improving\Events\Api\EventRepositoryInterface;
use Improving\Events\Ui\EntityUiManagerInterface;

class EventUiManager implements EntityUiManagerInterface
{
    /**
     * @var EventRepositoryInterface
     */
    private $repository;
    /**
     * @var EventFactory
     */
    public $factory;

    /**
     * @param EventRepositoryInterface $repository
     * @param EventFactory $factory
     */
    public function __construct(
        EventRepositoryInterface $repository,
        EventFactory $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param int|null $id
     * @return \Magento\Framework\Model\AbstractModel | Event | EventInterface;
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(?int $id)
    {
        return ($id)
            ? $this->repository->get($id)
            : $this->factory->create();
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $event
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magento\Framework\Model\AbstractModel $event)
    {
        $this->repository->save($event);
    }

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(int $id)
    {
        $this->repository->deleteById($id);
    }
}
