<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\SignupSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

class SignupSearchResults extends SearchResults implements SignupSearchResultsInterface
{

}
