<?php

declare(strict_types=1);

namespace Improving\Events\Model\ResourceModel;

use Improving\Events\Model\ResourceModel\AbstractModel;

class Event extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     * @codeCoverageIgnore
     * //phpcs:disable PSR2.Methods.MethodDeclaration.Underscore,PSR12.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init('events_event', 'event_id');
    }
    //phpcs: enable
}
