<?php

declare(strict_types=1);

namespace Improving\Events\Model\ResourceModel\Event;

use Improving\Events\Model\ResourceModel\Collection\AbstractCollection;

/**
 * @api
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore,PSR12.Classes.PropertyDeclaration.Underscore
     */
    protected $_idFieldName = 'event_id';
    //phpcs: enable

    /**
     * Define resource model
     *
     * @return void
     * @codeCoverageIgnore
     * //phpcs:disable PSR2.Methods.MethodDeclaration.Underscore,PSR12.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init(
            \Improving\Events\Model\Event::class,
            \Improving\Events\Model\ResourceModel\Event::class
        );
        //phpcs: enable
    }
}
