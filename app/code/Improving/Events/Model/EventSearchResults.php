<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\EventSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

class EventSearchResults extends SearchResults implements EventSearchResultsInterface
{

}
