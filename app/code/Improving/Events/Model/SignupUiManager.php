<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\SignupInterface;
use Improving\Events\Api\SignupRepositoryInterface;
use Improving\Events\Ui\EntityUiManagerInterface;

class SignupUiManager implements EntityUiManagerInterface
{
    /**
     * @var SignupRepositoryInterface
     */
    private $repository;
    /**
     * @var SignupFactory
     */
    public $factory;

    /**
     * @param SignupRepositoryInterface $repository
     * @param SignupFactory $factory
     */
    public function __construct(
        SignupRepositoryInterface $repository,
        SignupFactory $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param int|null $id
     * @return \Magento\Framework\Model\AbstractModel | Signup | SignupInterface;
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(?int $id)
    {
        return ($id)
            ? $this->repository->get($id)
            : $this->factory->create();
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $signup
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magento\Framework\Model\AbstractModel $signup)
    {
        $this->repository->save($signup);
    }

    /**
     * @param int $id
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(int $id)
    {
        $this->repository->deleteById($id);
    }
}
