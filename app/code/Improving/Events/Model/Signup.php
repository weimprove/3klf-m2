<?php

declare(strict_types=1);

namespace Improving\Events\Model;

use Improving\Events\Api\Data\SignupInterface;
use Improving\Events\Model\ResourceModel\Signup as SignupResourceModel;
use Magento\Framework\Model\AbstractExtensibleModel;

class Signup extends AbstractExtensibleModel implements SignupInterface
{
    /**
     * Cache tag
     *
     * @var string
     */
    public const CACHE_TAG = 'improving_events_signup';
    /**
     * Cache tag
     *
     * @var string
     * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore,PSR12.Classes.PropertyDeclaration.Underscore
     */
    protected $_cacheTag = self::CACHE_TAG;
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'improving_events_signup';
    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'signup';
    //phpcs:enable
    /**
     * Initialize resource model
     *
     * @return void
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore,PSR12.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init(SignupResourceModel::class);
        //phpcs:enable
    }

    /**
     * Get Signup id
     *
     * @return array
     */
    public function getSignupId()
    {
        return $this->getData(SignupInterface::SIGNUP_ID);
    }

    /**
     * Set Signup id
     *
     * @param  int $signupId
     * @return SignupInterface
     */
    public function setSignupId($signupId)
    {
        return $this->setData(self::SIGNUP_ID, $signupId);
    }

    /**
     *  Set Event Id
     *
     * @param int $eventId
     * @return SignupInterface
     */
    public function setEventId($eventId)
    {
        return $this->setData(self::EVENT_ID, $eventId);
    }

    /**
     * Get Event Id
     *
     * @return int
     */
    public function getEventId()
    {
        return $this->getData(self::EVENT_ID);
    }

    /**
     * Set Customer Id
     *
     * @param int $customerId
     * @return SignupInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get Customer Id
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set Signup
     *
     * @param string $signup
     * @return SignupInterface
     */
    public function setSignup($signup)
    {
        return $this->setData(self::SIGNUP, $signup);
    }

    /**
     * Get Signup
     *
     * @return string
     */
    public function getSignup()
    {
        return $this->getData(self::SIGNUP);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritdoc
     *
     * @return \Improving\Events\Api\Data\SignupExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritdoc
     *
     * @param \Improving\Events\Api\Data\SignupExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Improving\Events\Api\Data\SignupExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
