<?php

declare(strict_types=1);

namespace Improving\Events\Block\Event;

use Improving\Events\Api\Data\EventInterface;
use Improving\Events\Api\EventRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Name extends Template implements BlockInterface
{
    /**
     * @var EventRepositoryInterface
     */
    private $repository;
    /**
     * @var EventInterface
     */
    private $events = [];

    /**
     * Link constructor.
     * @param Template\Context $context
     * @param EventRepositoryInterface $repository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        EventRepositoryInterface $repository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->repository = $repository;
    }

    /**
     * @return bool|EventInterface
     */
    public function getEvent()
    {
        $eventId = (int)$this->getData('event_id');
        if (!array_key_exists($eventId, $this->events)) {
            try {
                $event = $this->repository->get($eventId);
                $this->events[$eventId] = $event;
                return $event;
            } catch (NoSuchEntityException $e) {
                $this->events[$eventId] = false;
                return false;
            }
        }
        return $this->events[$eventId];
    }

    /**
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $info = parent::getCacheKeyInfo();
        $info[] = $this->getData('event_id');
        return $info;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->getData('label');
    }
}
