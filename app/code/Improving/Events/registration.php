<?php

declare(strict_types=1);

use Magento\Framework\Component\ComponentRegistrar;

// @codeCoverageIgnoreStart
ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Improving_Events', __DIR__);
// @codeCoverageIgnoreEnd
